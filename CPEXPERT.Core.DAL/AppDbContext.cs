﻿using System;
using System.Collections.Generic;
using System.Linq;
using CPEXPERT.Common;
using CPEXPERT.Common.DAL.MsSql;
using CPEXPERT.Common.Enums;
using CPEXPERT.Core.Common.Domain.Entities;
using CPEXPERT.Core.Common.Enums;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace CPEXPERT.Core.DAL
{
	public class AppDbContext : DataBaseContext
	{

		public AppDbContext() : base(Log.Logger)
		{
		}

		public DbSet<UserEntity> Users { get; set; }
		public DbSet<ProfileEntity> Profiles { get; set; }
		public DbSet<PermissionEntity> Permissions { get; set; }
		public DbSet<RoleEntity> Roles { get; set; }
		public DbSet<SettingEntity> Settings { get; set; }
		public DbSet<SettingTypeEntity> SettingTypes { get; set; }
		public DbSet<OrganizationEntity> Organizations { get; set; }
		public DbSet<OfferEntity> Offers { get; set; }
		public DbSet<PaymentDetailEntity> PaymentDetails { get; set; }
		public DbSet<SubscriptionEntity> Subscriptions { get; set; }

		public override bool Seed()
		{
			try
			{
				#region Organizations
				// technical
				if (!Organizations.Any(a => a.AppKey == Statics.Organizations.Technical.Id))
				{
					Organizations.Add(new OrganizationEntity
					{
						AppKey = Statics.Organizations.Technical.Id,
						Name = Statics.Organizations.Technical.Name,
						Description = Statics.Organizations.Technical.Description,
						Type = Statics.Organizations.Technical.OrganizationType
					});
					base.SaveChanges();
				}

				// demo
				if (!Organizations.Any(a => a.AppKey == Statics.Organizations.Demo.Id))
				{
					Organizations.Add(new OrganizationEntity
					{
						AppKey = Statics.Organizations.Demo.Id,
						Name = Statics.Organizations.Demo.Name,
						Description = Statics.Organizations.Demo.Description,
						Type = Statics.Organizations.Demo.OrganizationType
					});
					base.SaveChanges();
				}

				// agency
				if (!Organizations.Any(a => a.AppKey == Statics.Organizations.Agency.Id))
				{
					Organizations.Add(new OrganizationEntity
					{
						AppKey = Statics.Organizations.Agency.Id,
						Name = Statics.Organizations.Agency.Name,
						Description = Statics.Organizations.Agency.Description,
						Type = Statics.Organizations.Agency.OrganizationType
					});
					base.SaveChanges();
				}
				#endregion

				#region Users
				// SA
				if (!Users.Any(a => a.EMail.Equals(Statics.Users.SuperAdmin.Email)))
				{
					CPEXPERT.Common.Helpers.PasswordHelper.CreatePasswordHash(Statics.Users.SuperAdmin.DefaultPassword, out var passwordHash, out var passwordSalt);
					var user = new UserEntity
					{
						AppKey = Statics.Users.SuperAdmin.Id,
						EMail = Statics.Users.SuperAdmin.Email,
						PasswordHash = passwordHash,
						PasswordSalt = passwordSalt,
						UserType = Statics.Users.SuperAdmin.UserType,
						Roles = new List<RoleEntity>(),
						Profile = new ProfileEntity
						{
							User = Users.SingleOrDefault(s => s.AppKey.Equals(Statics.Users.SuperAdmin.Id)),
							Avatar = null,
							FirstName = "Oskar",
							LastName = "Oleszkiewicz",
							PhoneNumber = "+48504733040"
						},
						OrganizationDbKey = Organizations.SingleOrDefault(s => s.AppKey == Statics.Organizations.Technical.Id).DbKey
					};
					Users.Add(user);
					base.SaveChanges();
				}

				// TECH
				if (!Users.Any(a => a.EMail.Equals(Statics.Users.TechSystemUser.Email)))
				{
					CPEXPERT.Common.Helpers.PasswordHelper.CreatePasswordHash(Statics.Users.TechSystemUser.DefaultPassword, out var passwordHash, out var passwordSalt);
					var user = new UserEntity
					{
						AppKey = Statics.Users.TechSystemUser.Id,
						EMail = Statics.Users.TechSystemUser.Email,
						PasswordHash = passwordHash,
						PasswordSalt = passwordSalt,
						UserType = Statics.Users.TechSystemUser.UserType,
						Roles = new List<RoleEntity>(),
						Profile = new ProfileEntity(),
						OrganizationDbKey = Organizations.SingleOrDefault(s => s.AppKey == Statics.Organizations.Technical.Id).DbKey
					};
					Users.Add(user);
					base.SaveChanges();
				}

				// DEMO
				if (!Users.Any(a => a.EMail.Equals(Statics.Users.DemoUser.Email)))
				{
					CPEXPERT.Common.Helpers.PasswordHelper.CreatePasswordHash(Statics.Users.DemoUser.DefaultPassword, out var passwordHash, out var passwordSalt);
					var user = new UserEntity
					{
						AppKey = Statics.Users.DemoUser.Id,
						EMail = Statics.Users.DemoUser.Email,
						PasswordHash = passwordHash,
						PasswordSalt = passwordSalt,
						UserType = Statics.Users.DemoUser.UserType,
						Roles = new List<RoleEntity>(),
						Profile = new ProfileEntity
						{
							User = Users.SingleOrDefault(s => s.AppKey.Equals(Statics.Users.DemoUser.Id)),
							Avatar = null,
							FirstName = "Demo",
							LastName = "Demo",
							PhoneNumber = "+48111222000"
						},
						OrganizationDbKey = Organizations.SingleOrDefault(s => s.AppKey == Statics.Organizations.Demo.Id).DbKey
					};
					Users.Add(user);
					base.SaveChanges();
				}

				// DEMO-ADMIN
				if (!Users.Any(a => a.EMail.Equals(Statics.Users.DemoAdminUser.Email)))
				{
					CPEXPERT.Common.Helpers.PasswordHelper.CreatePasswordHash(Statics.Users.DemoAdminUser.DefaultPassword, out var passwordHash, out var passwordSalt);
					var user = new UserEntity
					{
						AppKey = Statics.Users.DemoAdminUser.Id,
						EMail = Statics.Users.DemoAdminUser.Email,
						PasswordHash = passwordHash,
						PasswordSalt = passwordSalt,
						UserType = Statics.Users.DemoAdminUser.UserType,
						Roles = new List<RoleEntity>(),
						Profile = new ProfileEntity
						{
							User = Users.SingleOrDefault(s => s.AppKey.Equals(Statics.Users.DemoAdminUser.Id)),
							Avatar = null,
							FirstName = "Demo",
							LastName = "Admin",
							PhoneNumber = "+48222333111"
						},
						OrganizationDbKey = Organizations.SingleOrDefault(s => s.AppKey == Statics.Organizations.Demo.Id).DbKey
					};
					Users.Add(user);
					base.SaveChanges();
				}

				// AGENT
				if (!Users.Any(a => a.EMail.Equals(Statics.Users.AgentUser.Email)))
				{
					CPEXPERT.Common.Helpers.PasswordHelper.CreatePasswordHash(Statics.Users.AgentUser.DefaultPassword, out var passwordHash, out var passwordSalt);
					var user = new UserEntity
					{
						AppKey = Statics.Users.AgentUser.Id,
						EMail = Statics.Users.AgentUser.Email,
						PasswordHash = passwordHash,
						PasswordSalt = passwordSalt,
						UserType = Statics.Users.AgentUser.UserType,
						Roles = new List<RoleEntity>(),
						Profile = new ProfileEntity
						{
							User = Users.SingleOrDefault(s => s.AppKey.Equals(Statics.Users.AgentUser.Id)),
							Avatar = null,
							FirstName = "Martyna",
							LastName = "Agent",
							PhoneNumber = "+48222333111"
						},
						OrganizationDbKey = Organizations.SingleOrDefault(s => s.AppKey == Statics.Organizations.Agency.Id).DbKey
					};
					Users.Add(user);
					base.SaveChanges();
				}
				#endregion

				if (!SettingTypes.Any(a => a.SystemType == SettingSystemTypes.Custom))
				{
					SettingTypes.Add(new SettingTypeEntity
					{
						CreateDateUtc = DateTime.Now,
						CreateUser = Statics.Users.TechSystemUser.Id,
						IsSingle = false,
						SystemType = SettingSystemTypes.Custom,
					});
					base.SaveChanges();
				}

				if (!SettingTypes.Any(a => a.SystemType == SettingSystemTypes.FrontendEncryptedKey))
				{
					SettingTypes.Add(new SettingTypeEntity
					{
						CreateDateUtc = DateTime.Now,
						CreateUser = Statics.Users.TechSystemUser.Id,
						IsSingle = true,
						SystemType = SettingSystemTypes.FrontendEncryptedKey,
					});
					base.SaveChanges();
				}

				if (!Settings.Any(a => a.SettingKey == SettingSystemTypes.FrontendEncryptedKey.ToString()))
				{
					Settings.Add(new SettingEntity
					{
						ValueType = ValueTypes.String,
						SettingKey = SettingSystemTypes.FrontendEncryptedKey.ToString(),
						CreateDateUtc = DateTime.Now,
						CreateUser = Statics.Users.TechSystemUser.Id,
						Value = "9qjd8tdukFeM9C9f",
						SettingType = SettingTypes.SingleOrDefault(s => s.SystemType == SettingSystemTypes.FrontendEncryptedKey)
					});
					base.SaveChanges();
				}

				return true;
			}
			catch (Exception e)
			{
				Log.Error(e, "CPEXPERT.Core.DAL.AppDbContext");
				return false;
			}
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			#region Users

			modelBuilder.Entity<UserEntity>().ToTable("Users").HasKey(x => x.DbKey);
			modelBuilder.Entity<UserEntity>().Property(x => x.DbKey).ValueGeneratedOnAdd();
			modelBuilder.Entity<UserEntity>().HasIndex(x => x.AppKey).IsUnique();
			modelBuilder.Entity<UserEntity>().Property(x => x.AppKey).IsRequired();
			modelBuilder.Entity<UserEntity>().Property(x => x.CreateDateUtc).IsRequired();
			modelBuilder.Entity<UserEntity>().Property(x => x.CreateUser).IsRequired();
			modelBuilder.Entity<UserEntity>().Property(x => x.LastEditDateUtc).IsRequired();
			modelBuilder.Entity<UserEntity>().Property(x => x.LastEditUser).IsRequired();
			modelBuilder.Entity<UserEntity>().HasIndex(x => x.EMail).IsUnique();
			modelBuilder.Entity<UserEntity>().Property(x => x.EMail).IsRequired();
			modelBuilder.Entity<UserEntity>().Property(x => x.PasswordHash).IsRequired();
			modelBuilder.Entity<UserEntity>().Property(x => x.PasswordSalt).IsRequired();
			modelBuilder.Entity<UserEntity>().Property(x => x.UserType).IsRequired();
			modelBuilder.Entity<UserEntity>()
				.HasOne(x => x.Profile)
				.WithOne(x => x.User)
				.HasForeignKey<UserEntity>(x => x.ProfileDbKey)
				.IsRequired();
			modelBuilder.Entity<UserEntity>()
				.HasOne(x => x.Organization)
				.WithMany(x => x.Users)
				.HasForeignKey(x => x.OrganizationDbKey)
				.IsRequired();

			#endregion

			#region Profiles

			modelBuilder.Entity<ProfileEntity>().ToTable("Profiles").HasKey(x => x.DbKey);
			modelBuilder.Entity<ProfileEntity>().Property(x => x.DbKey).ValueGeneratedOnAdd();
			modelBuilder.Entity<ProfileEntity>().HasIndex(x => x.AppKey).IsUnique();
			modelBuilder.Entity<ProfileEntity>().Property(x => x.AppKey).IsRequired();
			modelBuilder.Entity<ProfileEntity>().Property(x => x.CreateDateUtc).IsRequired();
			modelBuilder.Entity<ProfileEntity>().Property(x => x.CreateUser).IsRequired();
			modelBuilder.Entity<ProfileEntity>().Property(x => x.LastEditDateUtc).IsRequired();
			modelBuilder.Entity<ProfileEntity>().Property(x => x.LastEditUser).IsRequired();
			modelBuilder.Entity<ProfileEntity>()
				.HasOne(x => x.User)
				.WithOne(x => x.Profile)
				.HasForeignKey<ProfileEntity>(x => x.UserDbKey)
				.IsRequired();

			#endregion

			#region Permissions

			modelBuilder.Entity<PermissionEntity>().ToTable("Permissions").HasKey(x => x.DbKey);
			modelBuilder.Entity<PermissionEntity>().Property(x => x.DbKey).ValueGeneratedOnAdd();
			modelBuilder.Entity<PermissionEntity>().HasIndex(x => x.AppKey).IsUnique();
			modelBuilder.Entity<PermissionEntity>().Property(x => x.AppKey).IsRequired();
			modelBuilder.Entity<PermissionEntity>().Property(x => x.CreateDateUtc).IsRequired();
			modelBuilder.Entity<PermissionEntity>().Property(x => x.CreateUser).IsRequired();
			modelBuilder.Entity<PermissionEntity>().Property(x => x.LastEditDateUtc).IsRequired();
			modelBuilder.Entity<PermissionEntity>().Property(x => x.LastEditUser).IsRequired();
			modelBuilder.Entity<PermissionEntity>().Property(x => x.PermissionName).IsRequired();
			modelBuilder.Entity<PermissionEntity>().Property(x => x.ElementCode).IsRequired();
			modelBuilder.Entity<PermissionEntity>().Property(x => x.ElementSecurityAccessSchema).IsRequired();

			#endregion

			#region Roles

			modelBuilder.Entity<RoleEntity>().ToTable("Roles").HasKey(x => x.DbKey);
			modelBuilder.Entity<RoleEntity>().Property(x => x.DbKey).ValueGeneratedOnAdd();
			modelBuilder.Entity<RoleEntity>().HasIndex(x => x.AppKey).IsUnique();
			modelBuilder.Entity<RoleEntity>().Property(x => x.AppKey).IsRequired();
			modelBuilder.Entity<RoleEntity>().Property(x => x.CreateDateUtc).IsRequired();
			modelBuilder.Entity<RoleEntity>().Property(x => x.CreateUser).IsRequired();
			modelBuilder.Entity<RoleEntity>().Property(x => x.LastEditDateUtc).IsRequired();
			modelBuilder.Entity<RoleEntity>().Property(x => x.LastEditUser).IsRequired();
			modelBuilder.Entity<RoleEntity>().Property(x => x.RoleName).IsRequired();

			#endregion

			#region Settings

			modelBuilder.Entity<SettingEntity>().ToTable("Settings").HasKey(x => x.DbKey);
			modelBuilder.Entity<SettingEntity>().Property(x => x.DbKey).ValueGeneratedOnAdd();
			modelBuilder.Entity<SettingEntity>().HasIndex(x => x.AppKey).IsUnique();
			modelBuilder.Entity<SettingEntity>().Property(x => x.AppKey).IsRequired();
			modelBuilder.Entity<SettingEntity>().Property(x => x.CreateDateUtc).IsRequired();
			modelBuilder.Entity<SettingEntity>().Property(x => x.CreateUser).IsRequired();
			modelBuilder.Entity<SettingEntity>().Property(x => x.LastEditDateUtc).IsRequired();
			modelBuilder.Entity<SettingEntity>().Property(x => x.LastEditUser).IsRequired();
			modelBuilder.Entity<SettingEntity>().HasIndex(x => x.SettingKey).IsUnique();
			modelBuilder.Entity<SettingEntity>().Property(x => x.SettingKey).IsRequired();
			modelBuilder.Entity<SettingEntity>().Property(x => x.ValueType).IsRequired();
			modelBuilder.Entity<SettingEntity>().Property(x => x.Value).IsRequired();
			modelBuilder.Entity<SettingEntity>()
				.HasOne(x => x.SettingType)
				.WithMany(x => x.Settings)
				.HasForeignKey(x => x.SettingTypeDbKey)
				.IsRequired();

			#endregion

			#region SettingSystemTypes

			modelBuilder.Entity<SettingTypeEntity>().ToTable("SettingSystemTypes").HasKey(x => x.DbKey);
			modelBuilder.Entity<SettingTypeEntity>().Property(x => x.DbKey).ValueGeneratedOnAdd();
			modelBuilder.Entity<SettingTypeEntity>().HasIndex(x => x.AppKey).IsUnique();
			modelBuilder.Entity<SettingTypeEntity>().Property(x => x.AppKey).IsRequired();
			modelBuilder.Entity<SettingTypeEntity>().Property(x => x.CreateDateUtc).IsRequired();
			modelBuilder.Entity<SettingTypeEntity>().Property(x => x.CreateUser).IsRequired();
			modelBuilder.Entity<SettingTypeEntity>().Property(x => x.LastEditDateUtc).IsRequired();
			modelBuilder.Entity<SettingTypeEntity>().Property(x => x.LastEditUser).IsRequired();
			modelBuilder.Entity<SettingTypeEntity>().HasIndex(x => x.SystemType).IsUnique();
			modelBuilder.Entity<SettingTypeEntity>().Property(x => x.SystemType).IsRequired();
			modelBuilder.Entity<SettingTypeEntity>().Property(x => x.IsSingle).IsRequired();

			#endregion

			#region Organization

			modelBuilder.Entity<OrganizationEntity>().ToTable("Organizations").HasKey(x => x.DbKey);
			modelBuilder.Entity<OrganizationEntity>().Property(x => x.DbKey).ValueGeneratedOnAdd();
			modelBuilder.Entity<OrganizationEntity>().HasIndex(x => x.AppKey).IsUnique();
			modelBuilder.Entity<OrganizationEntity>().Property(x => x.AppKey).IsRequired();
			modelBuilder.Entity<OrganizationEntity>().Property(x => x.CreateDateUtc).IsRequired();
			modelBuilder.Entity<OrganizationEntity>().Property(x => x.CreateUser).IsRequired();
			modelBuilder.Entity<OrganizationEntity>().Property(x => x.LastEditDateUtc).IsRequired();
			modelBuilder.Entity<OrganizationEntity>().Property(x => x.LastEditUser).IsRequired();
			modelBuilder.Entity<OrganizationEntity>().HasIndex(x => x.Name).IsUnique();
			modelBuilder.Entity<OrganizationEntity>().Property(x => x.Name).IsRequired();
			modelBuilder.Entity<OrganizationEntity>().Property(x => x.Description).IsRequired();
			modelBuilder.Entity<OrganizationEntity>().HasMany(x => x.Users);

			#endregion

			#region Offers

			modelBuilder.Entity<OfferEntity>().ToTable("Offers").HasKey(x => x.DbKey);
			modelBuilder.Entity<OfferEntity>().Property(x => x.DbKey).ValueGeneratedOnAdd();
			modelBuilder.Entity<OfferEntity>().HasIndex(x => x.AppKey).IsUnique();
			modelBuilder.Entity<OfferEntity>().Property(x => x.AppKey).IsRequired();
			modelBuilder.Entity<OfferEntity>().Property(x => x.CreateDateUtc).IsRequired();
			modelBuilder.Entity<OfferEntity>().Property(x => x.CreateUser).IsRequired();
			modelBuilder.Entity<OfferEntity>().Property(x => x.LastEditDateUtc).IsRequired();
			modelBuilder.Entity<OfferEntity>().Property(x => x.LastEditUser).IsRequired();
			modelBuilder.Entity<OfferEntity>().Property(x => x.Name).IsRequired();

			#endregion

			#region PaymentDetails

			modelBuilder.Entity<PaymentDetailEntity>().ToTable("PaymentDetails").HasKey(x => x.DbKey);
			modelBuilder.Entity<PaymentDetailEntity>().Property(x => x.DbKey).ValueGeneratedOnAdd();
			modelBuilder.Entity<PaymentDetailEntity>().HasIndex(x => x.AppKey).IsUnique();
			modelBuilder.Entity<PaymentDetailEntity>().Property(x => x.AppKey).IsRequired();
			modelBuilder.Entity<PaymentDetailEntity>().Property(x => x.CreateDateUtc).IsRequired();
			modelBuilder.Entity<PaymentDetailEntity>().Property(x => x.CreateUser).IsRequired();
			modelBuilder.Entity<PaymentDetailEntity>().Property(x => x.LastEditDateUtc).IsRequired();
			modelBuilder.Entity<PaymentDetailEntity>().Property(x => x.LastEditUser).IsRequired();
			modelBuilder.Entity<PaymentDetailEntity>().Property(x => x.PaymentMethod).IsRequired();
			modelBuilder.Entity<PaymentDetailEntity>().Property(x => x.PaymentMethodDetails).IsRequired();
			modelBuilder.Entity<PaymentDetailEntity>().Property(x => x.PaymentMethodIdentity).IsRequired();
			modelBuilder.Entity<PaymentDetailEntity>().Property(x => x.PaymentAutoRenew).IsRequired();
			modelBuilder.Entity<PaymentDetailEntity>().Property(x => x.PaymentDateTimeUtc).IsRequired();
			modelBuilder.Entity<PaymentDetailEntity>().Property(x => x.StartRenewingSubscription).IsRequired();
			modelBuilder.Entity<PaymentDetailEntity>().Property(x => x.RenewSubscriptionFrom).IsRequired();
			modelBuilder.Entity<PaymentDetailEntity>().Property(x => x.RenewSubscriptionTo).IsRequired();
			modelBuilder.Entity<PaymentDetailEntity>().Property(x => x.Amount).IsRequired();
			modelBuilder.Entity<PaymentDetailEntity>().Property(x => x.Currency).IsRequired();
			modelBuilder.Entity<PaymentDetailEntity>()
				.HasOne(x => x.Subscription)
				.WithMany(x => x.Payments)
				.HasForeignKey(x => x.SubscriptionDbKey)
				.IsRequired();

			#endregion

			#region Subscriptions

			modelBuilder.Entity<SubscriptionEntity>().ToTable("Subscriptions").HasKey(x => x.DbKey);
			modelBuilder.Entity<SubscriptionEntity>().Property(x => x.DbKey).ValueGeneratedOnAdd();
			modelBuilder.Entity<SubscriptionEntity>().HasIndex(x => x.AppKey).IsUnique();
			modelBuilder.Entity<SubscriptionEntity>().Property(x => x.AppKey).IsRequired();
			modelBuilder.Entity<SubscriptionEntity>().Property(x => x.CreateDateUtc).IsRequired();
			modelBuilder.Entity<SubscriptionEntity>().Property(x => x.CreateUser).IsRequired();
			modelBuilder.Entity<SubscriptionEntity>().Property(x => x.LastEditDateUtc).IsRequired();
			modelBuilder.Entity<SubscriptionEntity>().Property(x => x.LastEditUser).IsRequired();
			modelBuilder.Entity<SubscriptionEntity>().Property(x => x.StartDateTime).IsRequired();
			modelBuilder.Entity<SubscriptionEntity>().Property(x => x.RenewalDay).IsRequired();
			modelBuilder.Entity<SubscriptionEntity>().Property(x => x.DefaultPaymentMethod).IsRequired();
			modelBuilder.Entity<SubscriptionEntity>().Property(x => x.PaymentAutoRenew).IsRequired();
			modelBuilder.Entity<SubscriptionEntity>()
				.HasOne(x => x.Offer)
				.WithMany(x => x.Subscriptions)
				.HasForeignKey(f => f.OfferDbKey);
			modelBuilder.Entity<SubscriptionEntity>()
				.HasOne(x => x.Organization)
				.WithMany(x => x.Subscriptions)
				.HasForeignKey(f => f.OrganizationDbKey);

			#endregion

			base.OnModelCreating(modelBuilder);
		}
	}
}