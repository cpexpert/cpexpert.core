﻿using Autofac;
using CPEXPERT.Core.Common.DAL.Repositories;
using CPEXPERT.Core.DAL.Repositories;
using Serilog;

namespace CPEXPERT.Core.DAL.Autofac
{
	public class AutofacModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);
			builder.RegisterType<ProfilesRepositoryAsync>().As<IProfilesRepositoryAsync>();
			builder.RegisterType<SettingsRepositoryAsync>().As<ISettingsRepositoryAsync>();
			builder.RegisterType<UsersRepositoryAsync>().As<IUsersRepositoryAsync>();
			builder.RegisterType<OrganizationRepositoryAsync>().As<IOrganizationRepositoryAsync>();
			Log.Debug("[CPEXPERT.Core.DAL.Autofac].Load(...)");
		}
	}
}
