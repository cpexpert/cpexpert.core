﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CPEXPERT.Common.Abstracts;
using CPEXPERT.Common.ContractsData;
using CPEXPERT.Common.DAL.MsSql.Repositories;
using CPEXPERT.Core.Common.ContractsData.Organization;
using CPEXPERT.Core.Common.DAL.Repositories;
using CPEXPERT.Core.Common.Domain.Entities;
using Serilog;

namespace CPEXPERT.Core.DAL.Repositories
{
	public class OrganizationRepositoryAsync : BaseRepositoryAsync<OrganizationEntity>, IOrganizationRepositoryAsync
	{
		public OrganizationRepositoryAsync(AppDbContext context, ILogger logger, IMapper mapper, IUserDataProvider userDataProvider) : base(context, logger, mapper, userDataProvider)
		{
		}

		public Task<PaginatedListResponseData<OrganizationResponseData>> GetAllOrganizations()
		{
			var excludeOrganizations = new List<Guid>
			{
				CPEXPERT.Common.Statics.Organizations.Technical.Id
			};
			return GetFiltered<OrganizationResponseData>(x => !excludeOrganizations.Contains(x.AppKey));
		}

		public Task<OrganizationResponseData> GetOrganization(Guid organizationAppKey)
		{
			if (organizationAppKey == Guid.Empty)
			{
				throw new Exception("Organization AppKey is Guid.Empty");
			}

			return GetByAppKey<OrganizationResponseData>(organizationAppKey);
		}
	}
}