﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using CPEXPERT.Common.Abstracts;
using CPEXPERT.Common.DAL.MsSql.Repositories;
using CPEXPERT.Core.Common.DAL.Repositories;
using CPEXPERT.Core.Common.Domain.Entities;
using Serilog;

namespace CPEXPERT.Core.DAL.Repositories
{
	public class UsersRepositoryAsync : BaseRepositoryAsync<UserEntity>, IUsersRepositoryAsync
	{
		public UsersRepositoryAsync(AppDbContext context, ILogger logger, IMapper mapper, IUserDataProvider userDataProvider) : base(context, logger, mapper, userDataProvider)
		{
		}

		public Task<UserEntity> GetUser(string email)
		{
			if (string.IsNullOrEmpty(email))
			{
				throw new Exception("Email is null or empty");
			}

			return SingleOrDefault(s => s.EMail == email && s.Organization != null);
		}
	}
}