﻿using System.Threading.Tasks;
using AutoMapper;
using CPEXPERT.Common.Abstracts;
using CPEXPERT.Common.DAL.MsSql.Repositories;
using CPEXPERT.Core.Common.ContractsData.Setting;
using CPEXPERT.Core.Common.DAL.Repositories;
using CPEXPERT.Core.Common.Domain.Entities;
using CPEXPERT.Core.Common.Enums;
using Serilog;

namespace CPEXPERT.Core.DAL.Repositories
{
	public class SettingsRepositoryAsync : BaseRepositoryAsync<SettingEntity>, ISettingsRepositoryAsync
	{
		public SettingsRepositoryAsync(AppDbContext context, ILogger logger, IMapper mapper, IUserDataProvider userDataProvider) : base(context, logger, mapper, userDataProvider)
		{
		}

		public Task<SettingResponseData> GetFrontendEncryptedKey()
		{
			return SingleOrDefault<SettingResponseData>(s =>
				s.SettingKey.Equals(SettingSystemTypes.FrontendEncryptedKey.ToString()) &&
				s.SettingType.SystemType.Equals(SettingSystemTypes.FrontendEncryptedKey));
		}
	}
}