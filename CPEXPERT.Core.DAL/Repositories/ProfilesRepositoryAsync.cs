﻿using AutoMapper;
using CPEXPERT.Common.Abstracts;
using CPEXPERT.Common.DAL.MsSql.Repositories;
using CPEXPERT.Core.Common.DAL.Repositories;
using CPEXPERT.Core.Common.Domain.Entities;
using Serilog;


namespace CPEXPERT.Core.DAL.Repositories
{
	public class ProfilesRepositoryAsync : BaseRepositoryAsync<ProfileEntity>, IProfilesRepositoryAsync
	{
		public ProfilesRepositoryAsync(AppDbContext context, ILogger logger, IMapper mapper, IUserDataProvider userDataProvider) : base(context, logger, mapper, userDataProvider)
		{
		}
	}
}