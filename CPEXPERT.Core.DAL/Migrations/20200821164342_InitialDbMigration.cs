﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CPEXPERT.Core.DAL.Migrations
{
    public partial class InitialDbMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Offers",
                columns: table => new
                {
                    DbKey = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AppKey = table.Column<Guid>(nullable: false),
                    CreateDateUtc = table.Column<DateTime>(nullable: false),
                    CreateUser = table.Column<Guid>(nullable: false),
                    LastEditDateUtc = table.Column<DateTime>(nullable: false),
                    LastEditUser = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Offers", x => x.DbKey);
                });

            migrationBuilder.CreateTable(
                name: "Organizations",
                columns: table => new
                {
                    DbKey = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AppKey = table.Column<Guid>(nullable: false),
                    CreateDateUtc = table.Column<DateTime>(nullable: false),
                    CreateUser = table.Column<Guid>(nullable: false),
                    LastEditDateUtc = table.Column<DateTime>(nullable: false),
                    LastEditUser = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Organizations", x => x.DbKey);
                });

            migrationBuilder.CreateTable(
                name: "SettingSystemTypes",
                columns: table => new
                {
                    DbKey = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AppKey = table.Column<Guid>(nullable: false),
                    CreateDateUtc = table.Column<DateTime>(nullable: false),
                    CreateUser = table.Column<Guid>(nullable: false),
                    LastEditDateUtc = table.Column<DateTime>(nullable: false),
                    LastEditUser = table.Column<Guid>(nullable: false),
                    SystemType = table.Column<int>(nullable: false),
                    IsSingle = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SettingSystemTypes", x => x.DbKey);
                });

            migrationBuilder.CreateTable(
                name: "Subscriptions",
                columns: table => new
                {
                    DbKey = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AppKey = table.Column<Guid>(nullable: false),
                    CreateDateUtc = table.Column<DateTime>(nullable: false),
                    CreateUser = table.Column<Guid>(nullable: false),
                    LastEditDateUtc = table.Column<DateTime>(nullable: false),
                    LastEditUser = table.Column<Guid>(nullable: false),
                    StartDateTime = table.Column<DateTime>(nullable: false),
                    EndDateTime = table.Column<DateTime>(nullable: true),
                    RenewalDay = table.Column<int>(nullable: false),
                    OfferDbKey = table.Column<long>(nullable: false),
                    DefaultPaymentMethod = table.Column<int>(nullable: false),
                    DefaultPaymentMethodDetails = table.Column<string>(nullable: true),
                    PaymentAutoRenew = table.Column<bool>(nullable: false),
                    OrganizationDbKey = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subscriptions", x => x.DbKey);
                    table.ForeignKey(
                        name: "FK_Subscriptions_Offers_OfferDbKey",
                        column: x => x.OfferDbKey,
                        principalTable: "Offers",
                        principalColumn: "DbKey",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Subscriptions_Organizations_OrganizationDbKey",
                        column: x => x.OrganizationDbKey,
                        principalTable: "Organizations",
                        principalColumn: "DbKey",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    DbKey = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AppKey = table.Column<Guid>(nullable: false),
                    CreateDateUtc = table.Column<DateTime>(nullable: false),
                    CreateUser = table.Column<Guid>(nullable: false),
                    LastEditDateUtc = table.Column<DateTime>(nullable: false),
                    LastEditUser = table.Column<Guid>(nullable: false),
                    EMail = table.Column<string>(nullable: false),
                    PasswordHash = table.Column<byte[]>(nullable: false),
                    PasswordSalt = table.Column<byte[]>(nullable: false),
                    ProfileDbKey = table.Column<long>(nullable: false),
                    UserType = table.Column<int>(nullable: false),
                    OrganizationDbKey = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.DbKey);
                    table.ForeignKey(
                        name: "FK_Users_Organizations_OrganizationDbKey",
                        column: x => x.OrganizationDbKey,
                        principalTable: "Organizations",
                        principalColumn: "DbKey",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Settings",
                columns: table => new
                {
                    DbKey = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AppKey = table.Column<Guid>(nullable: false),
                    CreateDateUtc = table.Column<DateTime>(nullable: false),
                    CreateUser = table.Column<Guid>(nullable: false),
                    LastEditDateUtc = table.Column<DateTime>(nullable: false),
                    LastEditUser = table.Column<Guid>(nullable: false),
                    SettingKey = table.Column<string>(nullable: false),
                    ValueType = table.Column<int>(nullable: false),
                    Value = table.Column<string>(nullable: false),
                    SettingTypeDbKey = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Settings", x => x.DbKey);
                    table.ForeignKey(
                        name: "FK_Settings_SettingSystemTypes_SettingTypeDbKey",
                        column: x => x.SettingTypeDbKey,
                        principalTable: "SettingSystemTypes",
                        principalColumn: "DbKey",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PaymentDetails",
                columns: table => new
                {
                    DbKey = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AppKey = table.Column<Guid>(nullable: false),
                    CreateDateUtc = table.Column<DateTime>(nullable: false),
                    CreateUser = table.Column<Guid>(nullable: false),
                    LastEditDateUtc = table.Column<DateTime>(nullable: false),
                    LastEditUser = table.Column<Guid>(nullable: false),
                    PaymentMethod = table.Column<int>(nullable: false),
                    PaymentMethodDetails = table.Column<string>(nullable: false),
                    PaymentMethodIdentity = table.Column<string>(nullable: false),
                    PaymentAutoRenew = table.Column<bool>(nullable: false),
                    PaymentDateTimeUtc = table.Column<DateTime>(nullable: false),
                    StartRenewingSubscription = table.Column<DateTime>(nullable: false),
                    RenewSubscriptionFrom = table.Column<DateTime>(nullable: false),
                    RenewSubscriptionTo = table.Column<DateTime>(nullable: false),
                    Amount = table.Column<double>(nullable: false),
                    Currency = table.Column<int>(nullable: false),
                    SubscriptionDbKey = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentDetails", x => x.DbKey);
                    table.ForeignKey(
                        name: "FK_PaymentDetails_Subscriptions_SubscriptionDbKey",
                        column: x => x.SubscriptionDbKey,
                        principalTable: "Subscriptions",
                        principalColumn: "DbKey",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Profiles",
                columns: table => new
                {
                    DbKey = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AppKey = table.Column<Guid>(nullable: false),
                    CreateDateUtc = table.Column<DateTime>(nullable: false),
                    CreateUser = table.Column<Guid>(nullable: false),
                    LastEditDateUtc = table.Column<DateTime>(nullable: false),
                    LastEditUser = table.Column<Guid>(nullable: false),
                    Avatar = table.Column<byte[]>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    UserDbKey = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Profiles", x => x.DbKey);
                    table.ForeignKey(
                        name: "FK_Profiles_Users_UserDbKey",
                        column: x => x.UserDbKey,
                        principalTable: "Users",
                        principalColumn: "DbKey",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    DbKey = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AppKey = table.Column<Guid>(nullable: false),
                    CreateDateUtc = table.Column<DateTime>(nullable: false),
                    CreateUser = table.Column<Guid>(nullable: false),
                    LastEditDateUtc = table.Column<DateTime>(nullable: false),
                    LastEditUser = table.Column<Guid>(nullable: false),
                    RoleName = table.Column<string>(nullable: false),
                    UserEntityDbKey = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.DbKey);
                    table.ForeignKey(
                        name: "FK_Roles_Users_UserEntityDbKey",
                        column: x => x.UserEntityDbKey,
                        principalTable: "Users",
                        principalColumn: "DbKey",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Permissions",
                columns: table => new
                {
                    DbKey = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AppKey = table.Column<Guid>(nullable: false),
                    CreateDateUtc = table.Column<DateTime>(nullable: false),
                    CreateUser = table.Column<Guid>(nullable: false),
                    LastEditDateUtc = table.Column<DateTime>(nullable: false),
                    LastEditUser = table.Column<Guid>(nullable: false),
                    PermissionName = table.Column<string>(nullable: false),
                    ElementCode = table.Column<string>(nullable: false),
                    ElementSecurityAccessSchema = table.Column<string>(nullable: false),
                    RoleEntityDbKey = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Permissions", x => x.DbKey);
                    table.ForeignKey(
                        name: "FK_Permissions_Roles_RoleEntityDbKey",
                        column: x => x.RoleEntityDbKey,
                        principalTable: "Roles",
                        principalColumn: "DbKey",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Offers_AppKey",
                table: "Offers",
                column: "AppKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Organizations_AppKey",
                table: "Organizations",
                column: "AppKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Organizations_Name",
                table: "Organizations",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PaymentDetails_AppKey",
                table: "PaymentDetails",
                column: "AppKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PaymentDetails_SubscriptionDbKey",
                table: "PaymentDetails",
                column: "SubscriptionDbKey");

            migrationBuilder.CreateIndex(
                name: "IX_Permissions_AppKey",
                table: "Permissions",
                column: "AppKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Permissions_RoleEntityDbKey",
                table: "Permissions",
                column: "RoleEntityDbKey");

            migrationBuilder.CreateIndex(
                name: "IX_Profiles_AppKey",
                table: "Profiles",
                column: "AppKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Profiles_UserDbKey",
                table: "Profiles",
                column: "UserDbKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Roles_AppKey",
                table: "Roles",
                column: "AppKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Roles_UserEntityDbKey",
                table: "Roles",
                column: "UserEntityDbKey");

            migrationBuilder.CreateIndex(
                name: "IX_Settings_AppKey",
                table: "Settings",
                column: "AppKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Settings_SettingKey",
                table: "Settings",
                column: "SettingKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Settings_SettingTypeDbKey",
                table: "Settings",
                column: "SettingTypeDbKey");

            migrationBuilder.CreateIndex(
                name: "IX_SettingSystemTypes_AppKey",
                table: "SettingSystemTypes",
                column: "AppKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SettingSystemTypes_SystemType",
                table: "SettingSystemTypes",
                column: "SystemType",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Subscriptions_AppKey",
                table: "Subscriptions",
                column: "AppKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Subscriptions_OfferDbKey",
                table: "Subscriptions",
                column: "OfferDbKey");

            migrationBuilder.CreateIndex(
                name: "IX_Subscriptions_OrganizationDbKey",
                table: "Subscriptions",
                column: "OrganizationDbKey");

            migrationBuilder.CreateIndex(
                name: "IX_Users_AppKey",
                table: "Users",
                column: "AppKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_EMail",
                table: "Users",
                column: "EMail",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_OrganizationDbKey",
                table: "Users",
                column: "OrganizationDbKey");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PaymentDetails");

            migrationBuilder.DropTable(
                name: "Permissions");

            migrationBuilder.DropTable(
                name: "Profiles");

            migrationBuilder.DropTable(
                name: "Settings");

            migrationBuilder.DropTable(
                name: "Subscriptions");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "SettingSystemTypes");

            migrationBuilder.DropTable(
                name: "Offers");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Organizations");
        }
    }
}
