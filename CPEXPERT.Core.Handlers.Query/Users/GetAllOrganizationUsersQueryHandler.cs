﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.ContractsData;
using CPEXPERT.Common.ContractsData.Message;
using CPEXPERT.Common.Enums;
using CPEXPERT.Core.Common.ContractsData.Users;
using CPEXPERT.Core.Common.DAL.Repositories;
using CPEXPERT.Core.Common.Query.User;
using MediatR;
using Serilog;

namespace CPEXPERT.Core.Handlers.Query.Users
{
	public class GetAllOrganizationUsersQueryHandler : IRequestHandler<GetAllOrganizationUsersQuery, Response<PaginatedListResponseData<UserDetailedResponseData>>>
	{
		private readonly ILogger _logger;
		private readonly IUsersRepositoryAsync _repository;
		private readonly IMapper _mapper;
		public GetAllOrganizationUsersQueryHandler(ILogger logger, IUsersRepositoryAsync repository, IMapper mapper)
		{
			_logger = logger;
			_repository = repository;
			_mapper = mapper;
		}

		public async Task<Response<PaginatedListResponseData<UserDetailedResponseData>>> Handle(GetAllOrganizationUsersQuery query, CancellationToken cancellationToken)
		{
			var response = new Response<PaginatedListResponseData<UserDetailedResponseData>>
			{
				CanCache = query.CanCache,
				RequestCreated = query.RequestCreated,
				RequestId = query.RequestId
			};

			try
			{
				response.ResponseData = await _repository.GetFiltered<UserDetailedResponseData>(w => w.Organization.AppKey == query.QueryData);
			}
			catch (Exception e)
			{
				response.Messages.Add(new MessageData { Language = Languages.English, Message = e.Message, Type = MessageTypes.Error });
				_logger.Error(e, "GetAllOrganizationUsersQueryHandler error");
			}

			return response;
		}
	}
}