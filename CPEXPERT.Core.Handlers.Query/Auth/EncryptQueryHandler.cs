﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.ContractsData.Message;
using CPEXPERT.Common.Enums;
using CPEXPERT.Common.Helpers;
using CPEXPERT.Core.Common.DAL.Repositories;
using CPEXPERT.Core.Common.Query.Auth;
using MediatR;
using Serilog;

namespace CPEXPERT.Core.Handlers.Query.Auth
{
	public class EncryptQueryHandler : IRequestHandler<EncryptQuery, Response<string>>
	{
		private readonly ISettingsRepositoryAsync _settingsRepository;
		private readonly ILogger _logger;

		public EncryptQueryHandler(ILogger logger, ISettingsRepositoryAsync settingsRepository)
		{
			_logger = logger;
			_settingsRepository = settingsRepository;
		}

		public async Task<Response<string>> Handle(EncryptQuery query, CancellationToken cancellationToken)
		{
			var response = new Response<string>
			{
				CanCache = query.CanCache,
				RequestCreated = query.RequestCreated,
				RequestId = query.RequestId
			};

			try
			{
				var frontendEncryptedKey = await _settingsRepository.GetFrontendEncryptedKey();
				if (frontendEncryptedKey != null && !string.IsNullOrEmpty(frontendEncryptedKey.Value))
				{
					response.ResponseData = CryptographyHelper.TripleDES.Encoder(query.QueryData, frontendEncryptedKey.Value);
				}
				else
				{
					throw new Exception("Frontend encrypted key not found");
				}
			}
			catch (Exception e)
			{
				response.Messages.Add(new MessageData { Language = Languages.English, Message = e.Message, Type = MessageTypes.Error });
				_logger.Error(e, "EncryptQueryHandler error");
			}

			return response;
		}
	}
}