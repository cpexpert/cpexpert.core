﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.ContractsData.Auth;
using CPEXPERT.Common.ContractsData.Message;
using CPEXPERT.Common.Enums;
using CPEXPERT.Common.Helpers;
using CPEXPERT.Core.Common.DAL.Repositories;
using CPEXPERT.Core.Common.Query.Auth;
using MediatR;
using Serilog;

namespace CPEXPERT.Core.Handlers.Query.Auth
{
	public class GetLoginQueryHandler : IRequestHandler<GetLoginQuery, Response<AuthResponseData>>
	{
		private readonly ILogger _logger;
		private readonly IUsersRepositoryAsync _usersRepository;
		private readonly ISettingsRepositoryAsync _settingsRepository;
		private readonly IOrganizationRepositoryAsync _organizationRepository;

		public GetLoginQueryHandler(ILogger logger, IUsersRepositoryAsync usersRepository, ISettingsRepositoryAsync settingsRepository, IOrganizationRepositoryAsync organizationRepository)
		{
			_logger = logger;
			_usersRepository = usersRepository;
			_settingsRepository = settingsRepository;
			_organizationRepository = organizationRepository;
		}

		public async Task<Response<AuthResponseData>> Handle(GetLoginQuery query, CancellationToken cancellationToken)
		{
			var response = new Response<AuthResponseData>
			{
				CanCache = query.CanCache,
				RequestId = query.RequestId,
				RequestCreated = query.RequestCreated
			};

			try
			{
				if (query.QueryData != null && !string.IsNullOrEmpty(query.QueryData.Email) && !string.IsNullOrEmpty(query.QueryData.Password))
				{
					var frontendEncryptedKey = await _settingsRepository.GetFrontendEncryptedKey();
					var email = CryptographyHelper.TripleDES.Decoder(query.QueryData.Email, frontendEncryptedKey?.Value);
					var userOnDb = await _usersRepository.GetUser(email);
					if (userOnDb == null)
					{
						response.Messages.Add(new MessageData { Language = Languages.English, Message = "No user found on data store", Type = MessageTypes.Warning });
						return response;
					}

					var userOrganization = await _organizationRepository.SingleOrDefault(x => x.DbKey == userOnDb.OrganizationDbKey);
					if (userOrganization == null)
					{
						response.Messages.Add(new MessageData { Language = Languages.English, Message = "No organization found on data store", Type = MessageTypes.Warning });
						return response;
					}

					var password = CryptographyHelper.TripleDES.Decoder(query.QueryData.Password, frontendEncryptedKey?.Value);
					if (!PasswordHelper.VerifyPasswordHash(password, userOnDb.PasswordHash, userOnDb.PasswordSalt))
					{
						response.Messages.Add(new MessageData { Language = Languages.English, Message = "Wrong password", Type = MessageTypes.Warning });
						return response;
					}

					var systemAuthSecretKey = ConfigurationHelper.GetConfigSection<string>("JwtToken:SystemAuthSecretKey");
					var barerTokenValidPeriodHours = ConfigurationHelper.GetConfigSection<int>("JwtToken:ValidPeriodHours");
					if (string.IsNullOrEmpty(systemAuthSecretKey) || barerTokenValidPeriodHours <= 0)
					{
						throw new Exception("JwtToken configuration problem, no secret key or valid period is not set or is wrong");
					}

					response.ResponseData = AuthHelper.CreateAuthResponseData(
						userOnDb.AppKey,
						userOnDb.EMail, systemAuthSecretKey,
						new JwtData
						{
							UserAppKey = userOnDb.AppKey,
							UserType = userOnDb.UserType,
							Roles = new Role[0],
							CreateDate = DateTime.Now,
							ExpirationDate = DateTime.Now.AddHours(barerTokenValidPeriodHours),
							OrganizationAppKey = (userOnDb.UserType == SystemUserTypes.SuperAdministrator || userOnDb.UserType == SystemUserTypes.Tech) ? (Guid?)null : userOrganization.AppKey
						});

					if (response.ResponseData == null)
					{
						throw new Exception("Error when created JWT Barer Token, can't authenticate");
					}
				}
				else
				{
					throw new Exception("Request is not valid");
				}
			}
			catch (Exception e)
			{
				response.Messages.Add(new MessageData {Language = Languages.English, Message = e.Message, Type = MessageTypes.Error});
				_logger.Error(e, "GetLoginQueryHandler error");
			}

			return response;
		}
	}
}