﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.ContractsData.Message;
using CPEXPERT.Common.Enums;
using CPEXPERT.Core.Common.ContractsData.Organization;
using CPEXPERT.Core.Common.DAL.Repositories;
using CPEXPERT.Core.Common.Query.Organization;
using MediatR;
using Serilog;

namespace CPEXPERT.Core.Handlers.Query.Organizations
{
	public class GetOrganizationQueryHandler : IRequestHandler<GetOrganizationQuery, Response<OrganizationResponseData>>
	{
		private readonly ILogger _logger;
		private readonly IMapper _mapper;
		private readonly IOrganizationRepositoryAsync _organizationRepository;

		public GetOrganizationQueryHandler(ILogger logger, IMapper mapper, IOrganizationRepositoryAsync organizationRepository)
		{
			_logger = logger;
			_mapper = mapper;
			_organizationRepository = organizationRepository;
		}

		public async Task<Response<OrganizationResponseData>> Handle(GetOrganizationQuery query, CancellationToken cancellationToken)
		{
			var response = new Response<OrganizationResponseData>
			{
				CanCache = query.CanCache,
				RequestCreated = query.RequestCreated,
				RequestId = query.RequestId
			};

			try
			{
				response.ResponseData = await _organizationRepository.GetOrganization(query.QueryData);
				return response;
			}
			catch (Exception e)
			{
				response.Messages.Add(new MessageData { Language = Languages.English, Message = e.Message, Type = MessageTypes.Error });
				_logger.Error(e, "GetOrganizationQueryHandler error");
			}
			return response;
		}
	}
}