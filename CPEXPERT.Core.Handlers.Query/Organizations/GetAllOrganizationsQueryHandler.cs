﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.ContractsData;
using CPEXPERT.Common.ContractsData.Message;
using CPEXPERT.Common.Enums;
using CPEXPERT.Core.Common.ContractsData.Organization;
using CPEXPERT.Core.Common.DAL.Repositories;
using CPEXPERT.Core.Common.Query.Organization;
using MediatR;
using Serilog;

namespace CPEXPERT.Core.Handlers.Query.Organizations
{
	public class GetAllOrganizationsQueryHandler : IRequestHandler<GetAllOrganizationsQuery, Response<PaginatedListResponseData<OrganizationResponseData>>>
	{
		private readonly ILogger _logger;
		private readonly IOrganizationRepositoryAsync _organizationRepository;

		public GetAllOrganizationsQueryHandler(ILogger logger, IOrganizationRepositoryAsync organizationRepository)
		{
			_logger = logger;
			_organizationRepository = organizationRepository;
		}

		public async Task<Response<PaginatedListResponseData<OrganizationResponseData>>> Handle(GetAllOrganizationsQuery query, CancellationToken cancellationToken)
		{
			var response = new Response<PaginatedListResponseData<OrganizationResponseData>>
			{
				CanCache = query.CanCache,
				RequestCreated = query.RequestCreated,
				RequestId = query.RequestId
			};

			try
			{
				response.ResponseData = await _organizationRepository.GetAllOrganizations();
			}
			catch (Exception e)
			{
				response.Messages.Add(new MessageData { Language = Languages.English, Message = e.Message, Type = MessageTypes.Error });
				_logger.Error(e, "GetAllOrganizationsQueryHandler error");
			}
			return response;
		}
	}
}