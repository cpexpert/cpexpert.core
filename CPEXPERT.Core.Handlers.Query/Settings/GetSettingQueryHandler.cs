﻿using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.ContractsData.Message;
using CPEXPERT.Common.Enums;
using CPEXPERT.Core.Common.ContractsData.Setting;
using CPEXPERT.Core.Common.DAL.Repositories;
using CPEXPERT.Core.Common.Query.Setting;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;
using Serilog;

namespace CPEXPERT.Core.Handlers.Query.Settings
{
	public class GetSettingQueryHandler : IRequestHandler<GetSettingQuery, Response<SettingResponseData>>
	{
		private readonly ILogger _logger;
		private readonly ISettingsRepositoryAsync _repository;

		public GetSettingQueryHandler(ILogger logger, ISettingsRepositoryAsync repository)
		{
			_logger = logger;
			_repository = repository;
		}


		public async Task<Response<SettingResponseData>> Handle(GetSettingQuery query, CancellationToken cancellationToken)
		{
			var response = new Response<SettingResponseData>
			{
				CanCache = query.CanCache,
				RequestCreated = query.RequestCreated,
				RequestId = query.RequestId
			};

			try
			{
				var setting = await _repository.SingleOrDefault(s => s.SettingKey.Equals(query.QueryData));
				if (setting == null)
				{
					response.Messages.Add(new MessageData { Language = Languages.English, Message = "No setting found on data store", Type = MessageTypes.Warning });
					return response;
				}

				response.ResponseData = new SettingResponseData { ValueType = setting.ValueType, Key = setting.SettingKey, Value = setting.Value };
			}
			catch (Exception e)
			{
				response.Messages.Add(new MessageData { Language = Languages.English, Message = e.Message, Type = MessageTypes.Error });
				_logger.Error(e, "GetProfileDataQueryHandler error");
			}

			return response;
		}
	}
}