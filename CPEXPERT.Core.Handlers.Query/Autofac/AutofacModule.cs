﻿using Autofac;
using Serilog;

namespace CPEXPERT.Core.Handlers.Query.Autofac
{
	public class AutofacModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);
			Log.Debug("[CPEXPERT.Core.Handlers.Query.Autofac].Load(...)");
		}
	}
}
