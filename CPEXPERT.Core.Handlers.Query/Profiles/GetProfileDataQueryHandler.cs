﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.ContractsData.Message;
using CPEXPERT.Common.Enums;
using CPEXPERT.Core.Common.ContractsData.Profiles;
using CPEXPERT.Core.Common.DAL.Repositories;
using CPEXPERT.Core.Common.Domain.Entities;
using CPEXPERT.Core.Common.Query.Profile;
using MediatR;
using Serilog;

namespace CPEXPERT.Core.Handlers.Query.Profiles
{
	public class GetProfileDataQueryHandler : IRequestHandler<GetProfileDataQuery, Response<ProfileResponseData>>
	{
		private readonly ILogger _logger;
		private readonly IMapper _mapper;
		private readonly IProfilesRepositoryAsync _profilesRepository;

		public GetProfileDataQueryHandler(ILogger logger, IMapper mapper, IProfilesRepositoryAsync profilesRepository)
		{
			_logger = logger;
			_mapper = mapper;
			_profilesRepository = profilesRepository;
		}

		public async Task<Response<ProfileResponseData>> Handle(GetProfileDataQuery query, CancellationToken cancellationToken)
		{
			var response = new Response<ProfileResponseData>
			{
				CanCache = query.CanCache,
				RequestCreated = query.RequestCreated,
				RequestId = query.RequestId
			};

			try
			{
				var profile = await _profilesRepository.SingleOrDefault(s => s.User.AppKey == query.QueryData);

				if (profile == null)
				{
					response.Messages.Add(new MessageData { Language = Languages.English, Message = "Profile data not found", Type = MessageTypes.Warning });
					return response;
				}

				response.ResponseData = _mapper.Map<ProfileEntity, ProfileResponseData>(profile);
			}
			catch (Exception e)
			{
				response.Messages.Add(new MessageData { Language = Languages.English, Message = e.Message, Type = MessageTypes.Error });
				_logger.Error(e, "GetProfileDataQueryHandler error");
			}
			return response;
		}
	}
}