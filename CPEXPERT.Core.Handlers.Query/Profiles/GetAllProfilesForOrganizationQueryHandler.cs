﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.ContractsData;
using CPEXPERT.Common.ContractsData.Message;
using CPEXPERT.Common.Enums;
using CPEXPERT.Core.Common.ContractsData.Profiles;
using CPEXPERT.Core.Common.DAL.Repositories;
using CPEXPERT.Core.Common.Query.Profile;
using MediatR;
using Serilog;

namespace CPEXPERT.Core.Handlers.Query.Profiles
{
	public class GetAllProfilesForOrganizationQueryHandler : IRequestHandler<GetAllProfilesForOrganizationQuery, Response<PaginatedListResponseData<ProfileResponseData>>>
	{

		private readonly ILogger _logger;
		private readonly IProfilesRepositoryAsync _profilesRepository;

		public GetAllProfilesForOrganizationQueryHandler(ILogger logger, IProfilesRepositoryAsync profilesRepository)
		{
			_logger = logger;
			_profilesRepository = profilesRepository;
		}

		public async Task<Response<PaginatedListResponseData<ProfileResponseData>>> Handle(GetAllProfilesForOrganizationQuery query, CancellationToken cancellationToken)
		{
			var response = new Response<PaginatedListResponseData<ProfileResponseData>>
			{
				CanCache = query.CanCache,
				RequestCreated = query.RequestCreated,
				RequestId = query.RequestId
			};

			try
			{
				response.ResponseData = await _profilesRepository.GetFiltered<ProfileResponseData>(s =>
					s.User.Organization.AppKey == query.QueryData &&
					(s.User.UserType == SystemUserTypes.User || s.User.UserType == SystemUserTypes.Administrator)); 
			}
			catch (Exception e)
			{
				response.Messages.Add(new MessageData { Language = Languages.English, Message = e.Message, Type = MessageTypes.Error });
				_logger.Error(e, "GetAllProfilesForOrganizationQueryHandler error");
			}
			return response;
		}
	}
}