﻿using Autofac;
using Serilog;

namespace CPEXPERT.Core.Domain.Autofac
{
	public class AutofacModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);
			Log.Debug("[CPEXPERT.Core.Domain.Autofac].Load(...)");
		}
	}
}
