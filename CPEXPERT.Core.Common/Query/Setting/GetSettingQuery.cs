﻿using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.Query;
using CPEXPERT.Core.Common.ContractsData.Setting;

namespace CPEXPERT.Core.Common.Query.Setting
{
    public class GetSettingQuery : Query<string, Response<SettingResponseData>>
    {
    }
}