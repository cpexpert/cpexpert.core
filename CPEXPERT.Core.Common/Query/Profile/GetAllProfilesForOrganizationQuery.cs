﻿using System;
using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.ContractsData;
using CPEXPERT.Common.Query;
using CPEXPERT.Core.Common.ContractsData.Profiles;

namespace CPEXPERT.Core.Common.Query.Profile
{
	public class GetAllProfilesForOrganizationQuery : Query<Guid, Response<PaginatedListResponseData<ProfileResponseData>>>
	{
		
	}
}