﻿using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.Query;
using CPEXPERT.Core.Common.ContractsData.Profiles;
using System;

namespace CPEXPERT.Core.Common.Query.Profile
{
    public class GetProfileDataQuery : Query<Guid, Response<ProfileResponseData>>
    {
        
    }
}