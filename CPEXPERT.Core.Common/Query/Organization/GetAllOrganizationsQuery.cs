﻿using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.ContractsData;
using CPEXPERT.Common.Filters;
using CPEXPERT.Common.Query;
using CPEXPERT.Core.Common.ContractsData.Organization;

namespace CPEXPERT.Core.Common.Query.Organization
{
	public class GetAllOrganizationsQuery : Query<QueryFilterRequest, Response<PaginatedListResponseData<OrganizationResponseData>>>
	{
		
	}
}