﻿using CPEXPERT.Common.Query;
using System;
using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.ContractsData;
using CPEXPERT.Core.Common.ContractsData.Users;

namespace CPEXPERT.Core.Common.Query.User
{
	public class GetAllOrganizationUsersQuery : Query<Guid, Response<PaginatedListResponseData<UserDetailedResponseData>>>
	{
		
	}
}