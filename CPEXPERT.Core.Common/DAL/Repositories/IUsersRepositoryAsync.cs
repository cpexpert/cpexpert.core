﻿using System.Threading.Tasks;
using CPEXPERT.Common.DAL.Interfaces;
using CPEXPERT.Core.Common.Domain.Entities;

namespace CPEXPERT.Core.Common.DAL.Repositories
{
    public interface IUsersRepositoryAsync : IBaseRepositoryAsync<UserEntity, long>
    {
        Task<UserEntity> GetUser(string email);
    }
}