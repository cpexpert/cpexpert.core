﻿using CPEXPERT.Common.DAL.Interfaces;
using CPEXPERT.Core.Common.Domain.Entities;

namespace CPEXPERT.Core.Common.DAL.Repositories
{
    public interface IProfilesRepositoryAsync : IBaseRepositoryAsync<ProfileEntity, long>
    {
        
    }
}