﻿using System;
using System.Threading.Tasks;
using CPEXPERT.Common.ContractsData;
using CPEXPERT.Common.DAL.Interfaces;
using CPEXPERT.Core.Common.ContractsData.Organization;
using CPEXPERT.Core.Common.Domain.Entities;

namespace CPEXPERT.Core.Common.DAL.Repositories
{
	public interface IOrganizationRepositoryAsync : IBaseRepositoryAsync<OrganizationEntity, long>
	{
		Task<PaginatedListResponseData<OrganizationResponseData>> GetAllOrganizations();
		Task<OrganizationResponseData> GetOrganization(Guid organizationAppKey);
	}
}