﻿using System.Threading.Tasks;
using CPEXPERT.Common.DAL.Interfaces;
using CPEXPERT.Core.Common.ContractsData.Setting;
using CPEXPERT.Core.Common.Domain.Entities;

namespace CPEXPERT.Core.Common.DAL.Repositories
{
    public interface ISettingsRepositoryAsync : IBaseRepositoryAsync<SettingEntity, long>
    {
        Task<SettingResponseData> GetFrontendEncryptedKey();
    }
}