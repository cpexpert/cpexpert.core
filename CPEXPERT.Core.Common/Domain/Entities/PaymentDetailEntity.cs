﻿using System;
using CPEXPERT.Common.Domain.Entities;
using CPEXPERT.Core.Common.Enums;

namespace CPEXPERT.Core.Common.Domain.Entities
{
	public class PaymentDetailEntity : BaseEntity<long>
	{
		public PaymentMethods PaymentMethod { get; set; }
		public string PaymentMethodDetails { get; set; }
		public string PaymentMethodIdentity { get; set; }
		public bool PaymentAutoRenew { get; set; }
		public DateTime PaymentDateTimeUtc { get; set; }
		public DateTime StartRenewingSubscription { get; set; }
		public DateTime RenewSubscriptionFrom { get; set; }
		public DateTime RenewSubscriptionTo { get; set; }
		public double Amount { get; set; }
		public CurrencyTypes Currency { get; set; }
		public long SubscriptionDbKey { get; set; }
		public SubscriptionEntity Subscription { get; set; }
	}
}