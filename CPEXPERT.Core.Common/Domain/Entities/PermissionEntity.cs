﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using CPEXPERT.Common.Domain.Entities;
using CPEXPERT.Common.Enums;

namespace CPEXPERT.Core.Common.Domain.Entities
{
	public class PermissionEntity : BaseEntity<long>
	{
		public string PermissionName { get; set; }
		public string ElementCode { get; set; }
		public string ElementSecurityAccessSchema { get; set; }

		[NotMapped]
		public List<SecurityAccessTypes> ElementSecurityAccessSchemaList
		{
			get => !string.IsNullOrEmpty(ElementSecurityAccessSchema) ?
				ElementSecurityAccessSchema.Split(';').Select(s => (SecurityAccessTypes)Enum.Parse(typeof(SecurityAccessTypes), s)).ToList() : 
				new List<SecurityAccessTypes>();
			set => ElementSecurityAccessSchema = string.Join(';', value);
		}
	}
}