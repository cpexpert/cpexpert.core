﻿using System.Collections.Generic;
using CPEXPERT.Common.Domain.Entities;
using CPEXPERT.Common.Enums;

namespace CPEXPERT.Core.Common.Domain.Entities
{
	public class UserEntity : BaseEntity<long>
	{
		public UserEntity()
		{
			EMail = string.Empty;
			PasswordHash = null;
			PasswordSalt = null;
			Profile = new ProfileEntity();
		}

		public string EMail { get; set; }
		public byte[] PasswordHash { get; set; }
		public byte[] PasswordSalt { get; set; }
		public long ProfileDbKey { get; set; }
		public ProfileEntity Profile { get; set; }
		public SystemUserTypes UserType { get; set; }
		public long OrganizationDbKey { get; set; }
		public OrganizationEntity Organization { get; set; }
		public ICollection<RoleEntity> Roles { get; set; }
	}
}