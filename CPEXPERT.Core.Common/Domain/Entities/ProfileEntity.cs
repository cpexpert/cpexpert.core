﻿using CPEXPERT.Common.Domain.Entities;

namespace CPEXPERT.Core.Common.Domain.Entities
{
	public class ProfileEntity : BaseEntity<long>
	{
		public ProfileEntity()
		{
			Avatar = null;
			FirstName = string.Empty;
			LastName = string.Empty;
			PhoneNumber = string.Empty;
		}

		public byte[] Avatar { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string PhoneNumber { get; set; }
		public long UserDbKey { get; set; }
		public UserEntity User { get; set; }
	}
}