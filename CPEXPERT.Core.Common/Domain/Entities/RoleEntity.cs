﻿using System.Collections.Generic;
using CPEXPERT.Common.Domain.Entities;

namespace CPEXPERT.Core.Common.Domain.Entities
{
	public class RoleEntity : BaseEntity<long>
	{
		public string RoleName { get; set; }
		public ICollection<PermissionEntity> Permissions { get; set; }
	}
}