﻿using System.Collections.Generic;
using CPEXPERT.Common.Domain.Entities;

namespace CPEXPERT.Core.Common.Domain.Entities
{
	public class OfferEntity : BaseEntity<long>
	{
		public string Name { get; set; }
		public ICollection<SubscriptionEntity> Subscriptions { get; set; }
	}
}