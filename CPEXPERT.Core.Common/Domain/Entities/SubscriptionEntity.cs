﻿using System;
using System.Collections.Generic;
using CPEXPERT.Common.Domain.Entities;
using CPEXPERT.Core.Common.Enums;

namespace CPEXPERT.Core.Common.Domain.Entities
{
	public class SubscriptionEntity : BaseEntity<long>
	{
		public DateTime StartDateTime { get; set; }
		public DateTime? EndDateTime { get; set; }
		public int RenewalDay { get; set; }
		public long OfferDbKey { get; set; }
		public OfferEntity Offer { get; set; }
		public PaymentMethods DefaultPaymentMethod { get; set; }
		public string DefaultPaymentMethodDetails { get; set; }
		public bool PaymentAutoRenew { get; set; }
		public ICollection<PaymentDetailEntity> Payments { get; set; }
		public long OrganizationDbKey { get; set; }
		public OrganizationEntity Organization { get; set; }
	}
}