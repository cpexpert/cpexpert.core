﻿using System.Collections.Generic;
using CPEXPERT.Common.Domain.Entities;
using CPEXPERT.Common.Enums;
using CPEXPERT.Core.Common.Enums;

namespace CPEXPERT.Core.Common.Domain.Entities
{
	public class OrganizationEntity : BaseEntity<long>
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public SystemOrganizationTypes Type { get; set; }
		public ICollection<UserEntity> Users { get; set; }
		public ICollection<SubscriptionEntity> Subscriptions { get; set; }
	}
}