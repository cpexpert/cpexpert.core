﻿using System.Collections.Generic;
using CPEXPERT.Common.Domain.Entities;
using CPEXPERT.Core.Common.Enums;

namespace CPEXPERT.Core.Common.Domain.Entities
{
    public class SettingTypeEntity : BaseEntity<long>
    {
        public SettingSystemTypes SystemType { get; set; }
        public bool IsSingle { get; set; }
        public ICollection<SettingEntity> Settings { get; set; }
    }
}