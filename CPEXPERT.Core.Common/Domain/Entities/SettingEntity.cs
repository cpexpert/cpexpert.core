﻿using CPEXPERT.Common.Domain.Entities;
using CPEXPERT.Common.Enums;

namespace CPEXPERT.Core.Common.Domain.Entities
{
	public class SettingEntity : BaseEntity<long>
	{
		public string SettingKey { get; set; }
		public ValueTypes ValueType { get; set; }
		public string Value { get; set; }
		public long SettingTypeDbKey { get; set; }
		public SettingTypeEntity SettingType { get; set; }
	}
}