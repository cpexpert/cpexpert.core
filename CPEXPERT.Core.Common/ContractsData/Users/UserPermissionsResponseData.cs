﻿using CPEXPERT.Common.ContractsData.Auth;
using CPEXPERT.Common.Enums;

namespace CPEXPERT.Core.Common.ContractsData.Users
{
	public class UserPermissionsResponseData
	{
		public SystemUserTypes UserType { get; set; }
		public Role[] Roles { get; set; }
	}
}