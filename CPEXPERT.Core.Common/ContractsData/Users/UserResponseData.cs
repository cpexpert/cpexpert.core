﻿using System;

namespace CPEXPERT.Core.Common.ContractsData.Users
{
	public class UserResponseData
	{
		public Guid UserAppKey { get; set; }
		public string UserEmail { get; set; }
	}
}