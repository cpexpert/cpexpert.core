﻿using System;

namespace CPEXPERT.Core.Common.ContractsData.Users
{
	public class UserDetailedResponseData
	{
		public Guid AppKey { get; set; }
		public string AvatarWebSrc { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string PhoneNumber { get; set; }
		public string EMail { get; set; }
		public Guid ProfileAppKey { get; set; }
		public string FullName { get; set; }
	}
}