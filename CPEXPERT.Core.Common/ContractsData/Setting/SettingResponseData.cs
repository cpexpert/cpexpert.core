﻿using CPEXPERT.Common.Enums;

namespace CPEXPERT.Core.Common.ContractsData.Setting
{
	public class SettingResponseData
	{
		public string Key { get; set; }
		public ValueTypes ValueType { get; set; }
		public string Value { get; set; }
	}
}
