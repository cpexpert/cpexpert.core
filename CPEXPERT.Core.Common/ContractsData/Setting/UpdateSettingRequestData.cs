﻿using System;
using CPEXPERT.Common.Enums;

namespace CPEXPERT.Core.Common.ContractsData.Setting
{
	public class UpdateSettingRequestData
	{
		public Guid AppKey { get; set; }
		public string SettingKey { get; set; }
		public ValueTypes ValueType { get; set; }
		public string Value { get; set; }
	}
}