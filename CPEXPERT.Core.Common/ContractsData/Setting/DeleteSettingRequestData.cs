﻿using System;

namespace CPEXPERT.Core.Common.ContractsData.Setting
{
	public class DeleteSettingRequestData
	{
		public Guid AppKey { get; set; }
	}
}