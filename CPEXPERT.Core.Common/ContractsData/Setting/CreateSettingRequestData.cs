﻿using CPEXPERT.Common.Enums;

namespace CPEXPERT.Core.Common.ContractsData.Setting
{
	public class CreateSettingRequestData
	{
		public string SettingKey { get; set; }
		public ValueTypes ValueType { get; set; }
		public string Value { get; set; }
	}
}