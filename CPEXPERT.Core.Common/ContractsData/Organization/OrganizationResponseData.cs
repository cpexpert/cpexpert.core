﻿using System;

namespace CPEXPERT.Core.Common.ContractsData.Organization
{
	public class OrganizationResponseData
	{
		public Guid AppKey { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
	}
}