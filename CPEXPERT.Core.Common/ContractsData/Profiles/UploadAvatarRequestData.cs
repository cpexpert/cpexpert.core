﻿using System;

namespace CPEXPERT.Core.Common.ContractsData.Profiles
{
	public class UploadAvatarRequestData
	{
		public byte[] Avatar { get; set; }
		public Guid UserAppKey { get; set; }
	}
}