﻿using System;

namespace CPEXPERT.Core.Common.ContractsData.Profiles
{
	public class ProfileResponseData
	{
		public Guid AppKey { get; set; }
		public string AvatarWebSrc { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string PhoneNumber { get; set; }
		public string EMail { get; set; }
		public Guid UserAppKey { get; set; }
		public string FullName { get; set; }
	}
}