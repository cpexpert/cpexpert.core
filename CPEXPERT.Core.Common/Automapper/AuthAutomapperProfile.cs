﻿using AutoMapper;
using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.ContractsData.Auth;
using CPEXPERT.Core.Common.Query.Auth;

namespace CPEXPERT.Core.Common.Automapper
{
	public class AuthAutomapperProfile : Profile
	{
		public AuthAutomapperProfile()
		{
			CreateMap<Request<AuthRequestData>, GetLoginQuery>()
				.ForMember(dest => dest.RequestCreated, opt => opt.MapFrom(src => src.Created))
				.ForMember(dest => dest.CanCache, opt => opt.MapFrom(src => src.CanCache))
				.ForMember(dest => dest.RequestId, opt => opt.MapFrom(src => src.Id))
				.ForMember(dest => dest.QueryData, opt => opt.MapFrom(src => src.RequestData));
		}
	}
}
