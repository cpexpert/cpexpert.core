﻿using System;
using AutoMapper;
using CPEXPERT.Core.Common.ContractsData.Users;
using CPEXPERT.Core.Common.Domain.Entities;

namespace CPEXPERT.Core.Common.Automapper
{
	public class UserAutomapperProfile : Profile
	{
		public UserAutomapperProfile()
		{
			CreateMap<UserEntity, UserDetailedResponseData>()
				.ForMember(dest => dest.AppKey, opt => opt.MapFrom(src => src.AppKey))
				.ForMember(dest => dest.AvatarWebSrc, opt => opt.MapFrom(src => MapAvatarWebSrc(src.Profile)))
				.ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.Profile.FirstName))
				.ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.Profile.LastName))
				.ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.Profile.PhoneNumber))
				.ForMember(dest => dest.EMail, opt => opt.MapFrom(src => src.EMail))
				.ForMember(dest => dest.ProfileAppKey, opt => opt.MapFrom(src => src.Profile.AppKey))
				.ForMember(dest => dest.FullName, opt => opt.MapFrom(src => MapProfileFullName(src.Profile)));
		}

		private string MapProfileFullName(ProfileEntity src)
		{
			return $"{src.FirstName} {src.LastName}";
		}

		private string MapAvatarWebSrc(ProfileEntity src)
		{
			return src.Avatar != null && src.Avatar.Length > 0
				? "data:image/jpeg;base64," + Convert.ToBase64String(src.Avatar)
				: null;
		}
	}
}
