﻿using System;
using AutoMapper;
using CPEXPERT.Core.Common.ContractsData.Profiles;
using CPEXPERT.Core.Common.Domain.Entities;

namespace CPEXPERT.Core.Common.Automapper
{
	public class ProfileAutomapperProfile : Profile
	{
		public ProfileAutomapperProfile()
		{
			CreateMap<ProfileEntity, ProfileResponseData>()
				.ForMember(dest => dest.AppKey, opt => opt.MapFrom(src => src.AppKey))
				.ForMember(dest => dest.AvatarWebSrc, opt => opt.MapFrom(src => MapAvatarWebSrc(src)))
				.ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
				.ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
				.ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.PhoneNumber))
				.ForMember(dest => dest.EMail, opt => opt.MapFrom(src => src.User.EMail))
				.ForMember(dest => dest.UserAppKey, opt => opt.MapFrom(src => src.User.AppKey))
				.ForMember(dest => dest.FullName, opt => opt.MapFrom(src => MapProfileFullName(src)));
		}

		private string MapProfileFullName(ProfileEntity src)
		{
			return $"{src.FirstName} {src.LastName}";
		}

		private string MapAvatarWebSrc(ProfileEntity src)
		{
			return src.Avatar != null && src.Avatar.Length > 0
				? "data:image/jpeg;base64," + Convert.ToBase64String(src.Avatar)
				: null;
		}
	}
}
