﻿using AutoMapper;
using CPEXPERT.Core.Common.ContractsData.Setting;
using CPEXPERT.Core.Common.Domain.Entities;

namespace CPEXPERT.Core.Common.Automapper
{
	public class SettingAutomapperProfile : Profile
	{
		public SettingAutomapperProfile()
		{
			CreateMap<CreateSettingRequestData, SettingEntity>();
			CreateMap<UpdateSettingRequestData, SettingEntity>();
			CreateMap<SettingEntity, SettingResponseData>();
		}
	}
}
