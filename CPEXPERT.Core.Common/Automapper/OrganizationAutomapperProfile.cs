﻿using AutoMapper;
using CPEXPERT.Core.Common.ContractsData.Organization;
using CPEXPERT.Core.Common.Domain.Entities;

namespace CPEXPERT.Core.Common.Automapper
{
	public class OrganizationAutomapperProfile : Profile
	{
		public OrganizationAutomapperProfile()
		{
			CreateMap<OrganizationEntity, OrganizationResponseData>()
				.ForMember(dest => dest.AppKey, opt => opt.MapFrom(src => src.AppKey))
				.ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
				.ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description));
		}
	}
}