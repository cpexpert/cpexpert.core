﻿namespace CPEXPERT.Core.Common.Enums
{
	public enum PaymentMethods
	{
		BankTransfer,
		PayPal,
		CreditCard,
		DotPay,
	}
}