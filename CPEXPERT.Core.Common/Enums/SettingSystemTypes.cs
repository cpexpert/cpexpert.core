﻿namespace CPEXPERT.Core.Common.Enums
{
    public enum SettingSystemTypes
    {
        Custom = 0,
        FrontendEncryptedKey = 1,
    }
}