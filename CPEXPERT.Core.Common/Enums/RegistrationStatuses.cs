﻿namespace CPEXPERT.Core.Common.Enums
{
	public enum RegistrationStatuses
	{
		ActivationRequestSentByCustomer = 1,
		VerificationDataReceivedBySystem = 2,
		VerifyingBySystem = 3,
		ToImproveByCustomer = 4,
		AcceptedBySystem = 5,
		AcceptedByCustomer = 6,
		Activated = 7,
		RejectedBySystem = 8,
		RejectedByCustomer = 9
	}
}