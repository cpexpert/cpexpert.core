﻿using Autofac;
using CPEXPERT.Common.Event;
using CPEXPERT.Core.Common.Command.Test;
using CPEXPERT.Core.Handlers.Event.Tests;
using Rebus.Handlers;
using Serilog;

namespace CPEXPERT.Core.Handlers.Event.Autofac
{
	public class AutofacModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);
			//builder.RegisterType<TestCommandEventHandler>().As<IHandleMessages<EventMessage<TestCommand>>>();
			Log.Debug("[CPEXPERT.Core.Handlers.Event.Autofac].Load(...)");
		}
	}
}
