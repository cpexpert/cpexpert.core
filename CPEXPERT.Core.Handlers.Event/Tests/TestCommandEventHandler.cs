﻿using System.Threading.Tasks;
using CPEXPERT.Common.Event;
using CPEXPERT.Core.Common.Command.Test;
using Serilog;

namespace CPEXPERT.Core.Handlers.Event.Tests
{
	public class TestCommandEventHandler : EventMessageHandler<EventMessage<TestCommand>, TestCommand>
	{
		public TestCommandEventHandler(ILogger logger) : base(logger)
		{
		}

		public override Task Handle(EventMessage<TestCommand> eventMessage)
		{
			Logger.Debug("Execute TestCommandEventHandler");
			return Task.CompletedTask;
		}
	}
}