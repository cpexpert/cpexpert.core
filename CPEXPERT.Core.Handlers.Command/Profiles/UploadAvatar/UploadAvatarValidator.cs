﻿using System;
using CPEXPERT.Common.Helpers;
using CPEXPERT.Core.Common.ContractsData.Profiles;
using CPEXPERT.Core.Common.DAL.Repositories;
using FluentValidation;

namespace CPEXPERT.Core.Handlers.Command.Profiles.UploadAvatar
{
	public class UploadAvatarValidator : AbstractValidator<UploadAvatarRequestData>
	{
		public UploadAvatarValidator(IUsersRepositoryAsync usersRepository)
		{
			RuleFor(profile => profile)
				.Cascade(CascadeMode.Stop)
				.Must(x => x.Avatar != null && x.Avatar.Length > 0)
				.WithMessage("File is empty or null")
				.Must(x => x.UserAppKey != Guid.Empty)
				.WithMessage("User app key is empty")
				.Must(x => x.Avatar.GetSizeInMemoryInKB() <= 250)
				.WithMessage("Avatar must <= 250 kB")
				.MustAsync(async (data, token) =>
				{
					var user = await usersRepository.SingleOrDefault(s => s.AppKey == data.UserAppKey);
					return user?.Profile != null;
				}).WithMessage("Can't find user or profile on database");
		}
	}
}