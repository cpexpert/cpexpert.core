﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CPEXPERT.Common.Abstracts;
using CPEXPERT.Common.Command;
using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.ContractsData.Message;
using CPEXPERT.Common.Enums;
using CPEXPERT.Common.Event;
using CPEXPERT.Common.Service.SignalR;
using CPEXPERT.Core.Common.Command.Profile;
using CPEXPERT.Core.Common.ContractsData.Profiles;
using CPEXPERT.Core.Common.DAL.Repositories;
using FluentValidation;
using Serilog;

namespace CPEXPERT.Core.Handlers.Command.Profiles.UploadAvatar
{
	public class UploadAvatarCommandHandler : CommandHandler<UploadAvatarCommand, UploadAvatarRequestData>
	{
		public readonly IProfilesRepositoryAsync _profilesRepository;
		public readonly ISignalRSystemRemoteMethodsService _signalRSystemRemoteMethodsService;

		public UploadAvatarCommandHandler(IValidatorFactory validatorFactory, ILogger logger, IProfilesRepositoryAsync profilesRepository, ISignalRCommandService signalRCommandService, ISignalRSystemRemoteMethodsService signalRSystemRemoteMethodsService, IEventBus eventBus, IUserDataProvider userDataProvider) : 
			base(validatorFactory, logger, signalRCommandService, eventBus, userDataProvider)
		{
			_profilesRepository = profilesRepository;
			_signalRSystemRemoteMethodsService = signalRSystemRemoteMethodsService;
		}

		public override async Task<MessageData> Execute(UploadAvatarCommand command, CancellationToken cancellationToken)
		{
			var profile = await _profilesRepository.SingleOrDefault(s => s.User.AppKey == command.CommandData.UserAppKey);
			if (profile != null)
			{
				profile.Avatar = command.CommandData.Avatar;
				if (await _profilesRepository.Update(profile))
				{
					return new MessageData
					{
						Language = Languages.English,
						Message = "Update avatar SUCCESS",
						Type = MessageTypes.Information
					};
				}
			}

			throw new Exception("Avatar upload error");
		}

		public override Task StoreCommandOnEventStore(UploadAvatarCommand command)
		{
			return Task.CompletedTask;
		}

		public override async Task ExecuteSuccessAction(Guid executedUserAppKey, MessageData message)
		{
			await base.ExecuteSuccessAction(executedUserAppKey, message);
			await _signalRSystemRemoteMethodsService.RefreshUserProfile(new Request<Guid>
			{
				Language = Languages.English,
				CanCache = false,
				Created = DateTime.Now,
				Id = Guid.NewGuid(),
				RequestData = executedUserAppKey
			});
		}
	}
}