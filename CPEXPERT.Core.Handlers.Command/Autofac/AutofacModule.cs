﻿using System;
using Autofac;
using CPEXPERT.Core.Common.ContractsData.Profiles;
using CPEXPERT.Core.Common.ContractsData.Test;
using CPEXPERT.Core.Handlers.Command.Profiles.UploadAvatar;
using CPEXPERT.Core.Handlers.Command.Tests;
using FluentValidation;
using Serilog;

namespace CPEXPERT.Core.Handlers.Command.Autofac
{
	public class AutofacModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);
			builder.RegisterType<UploadAvatarValidator>().As<IValidator<UploadAvatarRequestData>>();
			builder.RegisterType<TestValidator>().As<IValidator<TestRequestData>>();
			Log.Debug("[CPEXPERT.Core.Handlers.Command.Autofac].Load(...)");
		}
	}
}
