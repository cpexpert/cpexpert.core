﻿using CPEXPERT.Core.Common.ContractsData.Test;
using FluentValidation;
using Serilog;

namespace CPEXPERT.Core.Handlers.Command.Tests
{
	public class TestValidator : AbstractValidator<TestRequestData>
	{
		public TestValidator(ILogger logger)
		{
			logger.Debug("Execute TestValidator");
		}
	}
}