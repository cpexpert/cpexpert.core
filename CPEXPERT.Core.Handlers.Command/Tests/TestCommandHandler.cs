﻿using System.Threading;
using System.Threading.Tasks;
using CPEXPERT.Common.Abstracts;
using CPEXPERT.Common.Command;
using CPEXPERT.Common.ContractsData.Message;
using CPEXPERT.Common.Enums;
using CPEXPERT.Common.Event;
using CPEXPERT.Common.Service.SignalR;
using CPEXPERT.Core.Common.Command.Test;
using CPEXPERT.Core.Common.ContractsData.Test;
using FluentValidation;
using Serilog;

namespace CPEXPERT.Core.Handlers.Command.Tests
{
	public class TestCommandHandler : CommandHandler<TestCommand, TestRequestData>
	{
		public TestCommandHandler(IValidatorFactory validatorFactory, ILogger logger, ISignalRCommandService signalRCommandService, IEventBus eventBus, IUserDataProvider userContextTask) : base(validatorFactory, logger, signalRCommandService, eventBus, userContextTask)
		{
		}

		public override Task<MessageData> Execute(TestCommand command, CancellationToken cancellationToken)
		{
			Logger.Debug("Execute TestCommandHandler.Execute(...)");
			return Task.FromResult(new MessageData
			{
				Language = Languages.English,
				Message = "TEST SUCCESS",
				Type = MessageTypes.Information
			});
		}

		public override Task StoreCommandOnEventStore(TestCommand command)
		{

			Logger.Debug("Execute TestCommandHandler.StoreCommandOnEventStore(...)");
			return Task.CompletedTask;
		}
	}
}