﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using AutoMapper;
using CPEXPERT.Common.Command;
using CPEXPERT.Common.Query;
using CPEXPERT.Common.WebApi.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace CPEXPERT.Core.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Authorize]
	public class LogsController : BaseController
	{
		public LogsController(IQueryBus queryBus, ICommandBus commandBus, ILogger logger, IMapper mapper) : base(queryBus, commandBus, logger, mapper)
		{
		}

		[HttpPost]
		[AllowAnonymous]
		public IActionResult Post([FromBody] NGXLogInterface request)
		{
			try
			{
				if (request?.Level != null)
				{
					switch (request.Level.Value)
					{
						case NgxLoggerLevel.TRACE:
							_logger.Information($"TRACE: {JsonSerializer.Serialize(request)}");
							break;
						case NgxLoggerLevel.DEBUG:
							_logger.Debug($"DEBUG: {JsonSerializer.Serialize(request)}");
							break;
						case NgxLoggerLevel.INFO:
							_logger.Information($"INFO: {JsonSerializer.Serialize(request)}");
							break;
						case NgxLoggerLevel.LOG:
							_logger.Information($"LOG: {JsonSerializer.Serialize(request)}");
							break;
						case NgxLoggerLevel.WARN:
							_logger.Warning($"WARN: {JsonSerializer.Serialize(request)}");
							break;
						case NgxLoggerLevel.ERROR:
							_logger.Error($"ERROR: {JsonSerializer.Serialize(request)}");
							break;
						case NgxLoggerLevel.FATAL:
							_logger.Fatal($"FATAL: {JsonSerializer.Serialize(request)}");
							break;
						case NgxLoggerLevel.OFF:
							_logger.Information($"OFF: {JsonSerializer.Serialize(request)}");
							break;
					}
				}
				return Ok();
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}
		}
	}

	public class NGXLogInterface
	{
		public NgxLoggerLevel? Level { get; set; }
		public DateTime? Timestamp { get; set; }
		public string FileName { get; set; }
		public string LineNumber { get; set; }
		public string Message { get; set; }
		public List<object> Additional { get; set; }
	}

	public enum NgxLoggerLevel
	{
		TRACE = 0,
		DEBUG = 1,
		INFO = 2,
		LOG = 3,
		WARN = 4,
		ERROR = 5,
		FATAL = 6,
		OFF = 7
	}
}
