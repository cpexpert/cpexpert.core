﻿using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.WebApi.Controllers;
using CPEXPERT.Core.Common.Query.Profile;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.IO;
using System.Threading.Tasks;
using AutoMapper;
using CPEXPERT.Common.Command;
using CPEXPERT.Common.Query;
using CPEXPERT.Core.Common.Command.Profile;
using CPEXPERT.Core.Common.ContractsData.Profiles;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace CPEXPERT.Core.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Authorize]
	public class ProfilesController : BaseController
	{
		public ProfilesController(IQueryBus queryBus, ICommandBus commandBus, ILogger logger, IMapper mapper) : base(queryBus, commandBus, logger, mapper)
		{ 
		}

		[HttpGet("GetProfileData"), DisableRequestSizeLimit]
		public async Task<IActionResult> GetProfileData([FromQuery] Guid userAppKey)
		{
			try
			{
				return Ok(await _queryBus.Execute(new GetProfileDataQuery
				{
					CanCache = false,
					RequestCreated = DateTime.Now,
					RequestId = Guid.NewGuid(),
					QueryData = userAppKey
				}));
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}
		}

		[HttpGet("GetAllProfilesForOrganization"), DisableRequestSizeLimit]
		public async Task<IActionResult> GetAllProfilesForOrganization([FromQuery] Guid organizationAppKey)
		{
			try
			{
				return Ok(await _queryBus.Execute(new GetAllProfilesForOrganizationQuery
				{
					CanCache = false,
					RequestCreated = DateTime.Now,
					RequestId = Guid.NewGuid(),
					QueryData = organizationAppKey
				}));
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}
		}

		[HttpPost("SetProfileData")]
		public async Task<IActionResult> SetProfileData([FromBody] Request<SetProfileRequestData> request)
		{
			try
			{
				return Ok(await _commandBus.ValidateAndPublish(new SetProfileDataCommand
				{
					CommandData = request.RequestData,
					RequestCreated = request.Created,
					RequestId = request.Id
				}));
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}
		}

		[HttpPost("UploadAvatar"), DisableRequestSizeLimit]
		public async Task<IActionResult> UploadAvatar(IFormFile avatar, [FromQuery] Guid userAppKey)
		{
			try
			{
				byte[] avatarFileBytes;
				await using (var memoryStream = new MemoryStream())
				{
					await avatar.CopyToAsync(memoryStream);
					avatarFileBytes = memoryStream.ToArray();
				}

				await _commandBus.ValidateAndPublish(new UploadAvatarCommand
				{
					CommandData = new UploadAvatarRequestData
					{
						Avatar = avatarFileBytes,
						UserAppKey = userAppKey
					},
					RequestCreated = DateTime.Now,
					RequestId = Guid.NewGuid(),
				});
				return Ok();
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}
		}
	}
}