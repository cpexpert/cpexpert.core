﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Serilog;
using System;
using System.Threading.Tasks;
using AutoMapper;
using CPEXPERT.Common.Command;
using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.ContractsData.Auth;
using CPEXPERT.Common.Query;
using CPEXPERT.Common.WebApi.Controllers;
using CPEXPERT.Core.Common.Query.Auth;

namespace CPEXPERT.Core.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	[Authorize]
	public class AuthController : BaseController
	{
		public AuthController(IQueryBus queryBus, ICommandBus commandBus, ILogger logger, IMapper mapper) : base(queryBus, commandBus, logger, mapper)
		{
		}

		[HttpPost("Login")]
		[AllowAnonymous]
		public async Task<IActionResult> Login([FromBody] Request<AuthRequestData> request)
		{
			try
			{
				return Ok(await _queryBus.Execute(_mapper.Map<Request<AuthRequestData>, GetLoginQuery>(request)));
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}
		}

		[HttpPost("Logout")]
		[AllowAnonymous]
		public IActionResult Logout([FromBody] Request<Guid> request)
		{
			try
			{
				return Ok();
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}
		}

		[HttpGet("Encrypt")]
		[AllowAnonymous]
		public async Task<IActionResult> Encrypt([FromQuery] string stringToEncrypted)
		{
			try
			{
				return Ok(await _queryBus.Execute(new EncryptQuery
				{
					CanCache = false,
					RequestCreated = DateTime.Now,
					RequestId = Guid.NewGuid(),
					QueryData = stringToEncrypted
				}));
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}
		}

		[HttpGet("ExtendAuthData")]
		[AllowAnonymous]
		public IActionResult ExtendAuthData([FromQuery] string encryptOldBarerToken)
		{
			try
			{
				return Ok();
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}
		}
	}
}