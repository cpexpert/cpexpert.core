﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using CPEXPERT.Common.Command;
using CPEXPERT.Common.Query;
using CPEXPERT.Common.WebApi.Controllers;
using CPEXPERT.Core.Common.Command.Test;
using CPEXPERT.Core.Common.ContractsData.Test;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace CPEXPERT.Core.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class TestController : BaseController
	{
		public TestController(IQueryBus queryBus, ICommandBus commandBus, ILogger logger, IMapper mapper) : base(queryBus, commandBus, logger, mapper)
		{
		}

		[HttpPost("ExecuteTestCommand")]
		[AllowAnonymous]
		public async Task<IActionResult> ExecuteTestCommand()
		{
			try
			{
				try
				{
					return Ok(await _commandBus.ValidateAndPublish(new TestCommand
					{
						CommandData = new TestRequestData
						{
							MyMessage = "IS OK WHEN EXECUTE!"
						},
						RequestCreated = DateTime.Now,
						RequestId = Guid.NewGuid()
					}));
				}
				catch (Exception e)
				{
					return BadRequest(e.Message);
				}
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}
		}
	}
}
