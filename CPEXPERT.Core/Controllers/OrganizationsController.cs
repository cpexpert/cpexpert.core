﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using CPEXPERT.Common.Command;
using CPEXPERT.Common.Filters;
using CPEXPERT.Common.Query;
using CPEXPERT.Common.WebApi.Controllers;
using CPEXPERT.Core.Common.Query.Organization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace CPEXPERT.Core.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	[Authorize]
	public class OrganizationsController : BaseController
	{
		public OrganizationsController(IQueryBus queryBus, ICommandBus commandBus, ILogger logger, IMapper mapper) : base(queryBus, commandBus, logger, mapper)
		{
		}

		[HttpGet("GetOrganization")]
		public async Task<IActionResult> GetOrganization([FromQuery] Guid organizationAppKey)
		{
			try
			{
				return Ok(await _queryBus.Execute(new GetOrganizationQuery
				{
					CanCache = false,
					RequestCreated = DateTime.Now,
					RequestId = Guid.NewGuid(),
					QueryData = organizationAppKey
				}));
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}
		}

		[HttpGet("GetAllOrganizations")]
		public async Task<IActionResult> GetAllOrganizations()
		{
			try
			{
				return Ok(await _queryBus.Execute(new GetAllOrganizationsQuery
				{
					CanCache = false,
					RequestCreated = DateTime.Now,
					RequestId = Guid.NewGuid(),
					QueryData = new QueryFilterRequest()
				}));
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}
		}
	}
}
