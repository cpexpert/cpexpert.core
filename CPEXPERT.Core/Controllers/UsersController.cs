﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using CPEXPERT.Common.Command;
using CPEXPERT.Common.Query;
using CPEXPERT.Common.WebApi.Controllers;
using CPEXPERT.Core.Common.Query.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace CPEXPERT.Core.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Authorize]
	public class UsersController : BaseController
	{
		public UsersController(IQueryBus queryBus, ICommandBus commandBus, ILogger logger, IMapper mapper) : base(queryBus, commandBus, logger, mapper)
		{
		}

		[HttpGet("GetAllOrganizationUsers"), DisableRequestSizeLimit]
		public async Task<IActionResult> GetAllOrganizationUsers([FromQuery] Guid organizationAppKey)
		{
			try
			{
				return Ok(await _queryBus.Execute(new GetAllOrganizationUsersQuery
				{
					CanCache = true,
					RequestCreated = DateTime.Now,
					RequestId = Guid.NewGuid(),
					QueryData = organizationAppKey
				}));
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}
		}
	}
}
