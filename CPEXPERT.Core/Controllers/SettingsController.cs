﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using CPEXPERT.Common.Command;
using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.Query;
using CPEXPERT.Common.WebApi.Controllers;
using CPEXPERT.Core.Common.Command.Setting;
using CPEXPERT.Core.Common.ContractsData.Setting;
using CPEXPERT.Core.Common.Query.Setting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace CPEXPERT.Core.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Authorize]
	public class SettingsController : BaseController
	{
		public SettingsController(IQueryBus queryBus, ICommandBus commandBus, ILogger logger, IMapper mapper) : base(queryBus, commandBus, logger, mapper)
		{

		}

		[AllowAnonymous]
		[HttpGet("GetFrontendEncryptedKey")]
		public async Task<IActionResult> GetFrontendEncryptedKey()
		{
			try
			{
				return Ok(await _queryBus.Execute(new GetSettingQuery
				{
					CanCache = false,
					RequestCreated = DateTime.Now,
					RequestId = Guid.NewGuid(),
					QueryData = "FrontendEncryptedKey"
				}));
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}
		}

		[HttpGet("GetSetting")]
		public async Task<IActionResult> GetSetting([FromQuery] string settingKey)
		{
			try
			{
				return Ok(await _queryBus.Execute(new GetSettingQuery
				{
					CanCache = false,
					RequestCreated = DateTime.Now,
					RequestId = Guid.NewGuid(),
					QueryData = settingKey
				}));
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}
		}

		[HttpPost()]
		public async Task<IActionResult> Create([FromBody] Request<CreateSettingRequestData> request)
		{
			try
			{
				return Ok(await _commandBus.ValidateAndPublish(new CreateSettingCommand
				{
					CommandData = request.RequestData,
					RequestCreated = request.Created,
					RequestId = request.Id
				}));
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}
		}

		[HttpPut]
		public async Task<IActionResult> Update([FromBody] Request<UpdateSettingRequestData> request)
		{
			try
			{
				return Ok(await _commandBus.ValidateAndPublish(new UpdateSettingCommand
				{
					CommandData = request.RequestData,
					RequestCreated = request.Created,
					RequestId = request.Id
				}));
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}
		}

		[HttpDelete("{appKey}")]
		public async Task<IActionResult> Delete(Guid appKey)
		{
			try
			{
				return Ok(await _commandBus.ValidateAndPublish(new DeleteSettingCommand
				{
					CommandData = new DeleteSettingRequestData { AppKey = appKey },
					RequestCreated = DateTime.Now,
					RequestId = Guid.NewGuid()
				}));
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}
		}
	}
}
