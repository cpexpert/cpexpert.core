﻿using Autofac;
using Serilog;

namespace CPEXPERT.Core.Autofac
{
	public class AutofacModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);
			Log.Debug("[CPEXPERT.Core.Autofac].Load(...)");
		}
	}
}
