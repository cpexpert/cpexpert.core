using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using CPEXPERT.Common.Event.AppSettings;
using CPEXPERT.Core.DAL;
using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Serilog;
using Rebus.Config;
using Rebus.Retry.Simple;
using Rebus.Serialization.Json;
using Rebus.ServiceProvider;

namespace CPEXPERT.Core
{
	public class Startup
	{
		public Startup(IHostEnvironment environment)
		{
			_configuration = new ConfigurationBuilder()
				.AddJsonFile("appsettings.json", false, true)
				.AddJsonFile($"appsettings.{environment.EnvironmentName}.json", true)
				.AddEnvironmentVariables()
				.Build();
		}

		public readonly IConfiguration _configuration;
		public IContainer ApplicationContainer { get; private set; }

		public IServiceProvider ConfigureServices(IServiceCollection services)
		{
			try
			{
				Log.Logger = new LoggerConfiguration()
					.ReadFrom.Configuration(_configuration)
					.CreateLogger();
				Log.Information("Application started...");

				var builder = new ContainerBuilder();

				var appAssemblies = new List<Assembly>(Directory
					.EnumerateFiles(Path.GetDirectoryName(Assembly.GetEntryAssembly()?.Location), "*.dll",
						SearchOption.AllDirectories)
					.Where(fullFilePath => Path.GetFileName(fullFilePath).StartsWith("CPEXPERT.")).ToList()
					.Select(Assembly.LoadFrom));

				appAssemblies.ForEach(assembly => builder.RegisterAssemblyModules(assembly));
				services.AddMvc()
					.AddFluentValidation(fvc => fvc.RegisterValidatorsFromAssemblyContaining<Startup>())
					.AddJsonOptions(options => {
						options.JsonSerializerOptions.PropertyNamingPolicy = null;
						options.JsonSerializerOptions.PropertyNameCaseInsensitive = false;
					});
				services.AddMemoryCache();
				services.AddResponseCaching();
				services.AddSingleton(Log.Logger);
				services.AddControllers();
				services.AddCors(options =>
				{
					options.AddPolicy("CorsPolicy", corsPolicyBuilder => corsPolicyBuilder
						.WithOrigins(_configuration.GetSection("Cors:Origins").Value.Split(';'))
						.AllowAnyMethod()
						.AllowAnyHeader()
						.AllowCredentials());
				});

				services.AddMediatR(Assembly.GetExecutingAssembly());
				services.AddDbContext<AppDbContext>();

				var rebusConfigSection = _configuration.GetSection("Rebus").Get<RebusSettings>();
				services.AddRebus((configure, context) => configure
					.Logging(l => l.Serilog())
					.Transport(t => t.UseRabbitMq(rebusConfigSection.ConnectionString, rebusConfigSection.InputQueue))
					.Serialization(s => s.UseNewtonsoftJson(new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.None }))
					.Options(o =>
					{
						o.SimpleRetryStrategy(maxDeliveryAttempts: 10);
						o.SetMaxParallelism(rebusConfigSection.MaxParallelism);
						o.SetNumberOfWorkers(rebusConfigSection.NumberOfWorkers);
					})
				);

				services.AddAuthentication(cfg =>
				{
					cfg.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
					cfg.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
					cfg.DefaultSignInScheme = JwtBearerDefaults.AuthenticationScheme;
				}).AddJwtBearer(cfg =>
				{
					cfg.RequireHttpsMetadata = false;
					cfg.SaveToken = true;
					cfg.TokenValidationParameters = new TokenValidationParameters
					{
						ValidateIssuer = false,
						ValidateAudience = false,
						ValidateLifetime = true,
						IssuerSigningKey = new SymmetricSecurityKey(
							Encoding.UTF8.GetBytes(_configuration.GetSection("JwtToken:SystemAuthSecretKey").Value))
					};
				});
				services.AddAutoMapper(appAssemblies);
				services.AddSwaggerGen();

				builder.Populate(services);
				ApplicationContainer = builder.Build();
				return new AutofacServiceProvider(ApplicationContainer);
			}
			catch (Exception e)
			{
				Log.Error(e, "Error when start WebAPI");
				throw;
			}
		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseHttpsRedirection();
			app.UseRouting();
			app.UseCors("CorsPolicy");
			app.UseAuthentication();
			app.UseAuthorization();
			app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

			app.UseSwagger();
			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
			});

			MigrateAndSeedDataBase(app);
			RegisterSubscribers(app);

			Log.Information("Application start SUCCESS");
		}

		private static void MigrateAndSeedDataBase(IApplicationBuilder app)
		{
			using var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
			using var dbContext = serviceScope.ServiceProvider.GetService<AppDbContext>();
			if (dbContext.Migrate())
			{
				dbContext.Seed();
			}
		}

		private static void RegisterSubscribers(IApplicationBuilder app)
		{
			//test
			//app.ApplicationServices.UseRebus(async bus => await bus.Subscribe<EventMessage<TestCommand>>());
		}
	}
}
